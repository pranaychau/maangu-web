<?php 
$db=new DB();
$str="";
$msg=loadvariable('msg','');
$a=loadvariable('a','list');
$id = loadvariable('id','');
$photo = loadvariable('photo','');
if($msg=='1')
{
		$str="Successfully Updated ";
		$cls="alert alert-success";
}
if($msg=='0')
{
		$str="Not Updated ....Problem Occured ";
		$cls="alert alert-danger";
}
if($a=='list')
{
	/*$SQL="SELECT * FROM `report`";*/
	$SQL="select report.*,members.photo_id from report inner join members on members.id = report.report_id";
	$res=$db->get_results($SQL);
}
?> 
	<script>
		$(document).ready(function(){
		    $('#myTable').dataTable();
		    //myTable is you table id.
		});
	</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
	  <section class="content">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           Report
            <small>Report</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
               <li><a href="index.php?p=users">Users</a></li>
			<li class="active">Report</li>  	
          </ol>
        </section>
      <div class="content">
         <!-- Main content -->
          <div class="row">
            <div class="col-xs-12">
		<?php 
			if($str!="")
			{?>
			<div class="<?php echo  $cls;?>" role="alert">
			    <a href="#" class="alert-link"><?php  echo $str;?></a>
			</div>
			<?php }
			?>
                <div class="box-header">
                  <h3 class="box-title"></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
				<?php if($a=='list')
				{?>
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                      	<th style="text-align: center;">Sr. no.</th>
                        <th style="text-align: center;">Reporting Member</th>
                        <th style="text-align: center;">Reported Member</th>
                        <th style="text-align: center;">Profile Pic</th>
                        <th style="text-align: center;">Action</th>
                      </tr>
                    </thead>
                    <tbody>
					<?php for($i=0;$i<count($res);$i++)
					{?>
                      <tr>
                     <td style="text-align: center;"> <?php echo $i+1; ?></td>
					 <td style="text-align: center;"> <?php echo get_member_name($res[$i]['member_id']); ?></td>
					  <td style="text-align: center;"> <?php echo get_member_name($res[$i]['report_id']); ?></td>
					  <td style="text-align: center;"><img style="width: 150px;height: 150px;" class="media-object" src = <?php echo "https://www.maangu.com/mobile/upload/".$res[$i]['profile_pic']; ?>></td>
					   <td style="text-align: center;">
					   	<?php if($res[$i]['status']=='1')
						{?>
					   <a class="btn btn-danger" href="../model/report.php?a=delete_photo&photo_member_id=<?php echo $res[$i]['report_id'];?>&photo_id=<?php echo $res[$i]['photo_id'];?>&status=0&id=<?php echo $res[$i]['id'];?>&profile_pic=<?php echo $res[$i]['profile_pic'];?>"><i class="fa fa-remove"></i>Delete Photo</a>
					   <?php } else { ?>
						 <a class="btn btn-success"  href="#" title="Deleted"><i class="fa fa-check"></i></a>
						<?php } ?>
					   <a class="btn btn-danger"href="../model/report.php?a=delete&id=<?php echo $res[$i]['id']; ?>"><i class="fa  fa-remove"></i>Delete Entry</a>
					   </td>
                      </tr>
					  <?php }?>
                     </tbody>
                  </table>
				  <?php }?><!-----LIST CLOSE----------------->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
		  </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper --><!-- /.content -->
     
  
