<?php 
$db=new DB();
$str="";
$start=loadvariable('start','0');
$end=30;
$msg=loadvariable('msg','');
$action=loadvariable('action','');
$a=loadvariable('a','list');
if($msg=='1')
{
		$str="Successfully Updated ";
		$cls="alert alert-success";
}
if($msg=='0')
{
		$str="Not Updated ....Problem Occured ";
		$cls="alert alert-danger";
}
if($msg==10)
{
$str="SuccessFully waived user for 1 month ";
$cls="alert alert-success";

}
if($action=='search')
{
$val=loadvariable('val','');
$option=loadvariable('option','');
$SQL="select members.*, users.* from users inner join members on users.member_id=members.id WHERE users.email LIKE '%$val%'";

$res=$db->get_results($SQL);

}
?>
   
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
	  <section class="content">
	   <?php if($a=='list')
			{ ?>
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>Payment Management</h1>
      
          <ol class="breadcrumb">
	  <li><a href="index.php"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="index.php?p=payments">Payments</a></li> 	
          </ol>
		  <?php// echo  $SQL;?>
        </section>
      <div class="content">
         <!-- Main content -->
                <?php 
				if($str!="")
				{?>
				<div class="<?php echo  $cls;?>" role="alert">
			  <a href="#" class="alert-link"><?php  echo $str;?></a>
			</div>
			<?php }
			?> 
          <div class="row">
            <div class="content">
      <!----------------------start search box---->
              <div class="box">
                 <div class="col-lg-12">
							<form class="navbar-form  pull-center" action="index.php"  role="search">
							<input type="hidden" name="p" value="payments">
							<input type="hidden" name="action" value="search">
                <div class="input-group"><h4><b style="font-weight:700"> User Email Id: </b></h4></div>
                <div class="input-group">
                <input type="email" class="form-control" size="20" placeholder="Email ID" name="val">
                </div>
				 
			    <div class="input-group">
                    <input type="submit" class="form-control btn btn-primary" value="Search">
                </div>
				</form>
				         </div>
                       </div>
                     <!----------end search------------------------>
					 
                <div class="box-body">
                  <table id="example" style="font-size:14px" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th style="text-align: center;">Sr. no.</th>
                        <th style="text-align: center;">Name</th>
                        <th style="text-align: center;">Sex</th>
                        <th style="text-align: center;">Age</th>
                        <th style="text-align: center;">Mobile no.</th>
            						<th style="text-align: center;">Payment status</th>
            						<th style="text-align: center;">Email</th>
                        <th style="text-align: center;">Address</th>
                        </tr>
                    </thead>
                  <tbody>
                        <?php  $date1=date('Y-m-d');
						            for($i=0;$i<count($res);$i++) { ?>
                        
						                <tr>
                                <td style="text-align: center;"><?php echo $start+$i+1; ?></td>
                                <td style="text-align: center;"><?php echo  $res[$i]['first_name']." ".$res[$i]['last_name'] ?></td>
                                <td style="text-align: center;"><?php echo ucwords($res[$i]['sex']);?></td>
                                <td style="text-align: center;"><?php member_age($res[$i]['birthday']);?></td>
                                <td style="text-align: center;"> <?php echo $res[$i]['phone_number'];?></td>
								                <td style="text-align: center;"> <?php
								                  $date2=date("Y-m-d", strtotime($res[$i]['payment_expires']));
								                    if(strtotime($date1) < strtotime($date2))
                    								{  echo  "<br>".$date2;?>
                      									<button class="btn btn-success" style="padding:2px;" href="#"><i class="fa fa-check"></i>validity</button>
                      					<?php } else {?>
  								                       <button class="btn btn-danger" style="padding:2px;" href="#"><i class="fa fa-check"></i>expired</button>
                          									<?php echo  "<br>".$date2;
                          								}?>
								                </td>
								                <td style="text-align: center;"> <?php echo $res[$i]['email'];?></td>
                                <td style="text-align: center;"><?php echo  $res[$i]['city']." <br>".$res[$i]['district']."<br>".$res[$i]['state']."<br> ".$res[$i]['country']; ?></td> 
                               
                          </tr>
                          <tr>
                          <td colspan="5"><button class="btn btn-success btn-lg pull-right" data-toggle="modal" data-target="#myModal"> Paid</button></td>
                        </tr>


                          <!----------------------------------Start model for pop up form-------------------------->
                  <div class="modal fade" id="myModal" role="dialog">
                      <div class="modal-dialog">
                      
                          <!-- Modal content-->
                          <div class="modal-content">
                              <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Update details</h4>
                              </div>
                              <div class="modal-body">
                                <form role="form" method="post" action="../model/payment.php">
                                      <input type="hidden" name="id" value="<?php echo $res[$i]['id'];?>">
                                      <input type="hidden" name="admin_id" value="<?php echo $_SESSION['Id'];?>">
                                    <div class="form-group">
                                      <label for="email">Name</label>
                                      <input type="text" name="name" value="<?php echo $res[$i]['first_name']." ".$res[$i]['last_name'];?>" class="form-control" id="email">
                                    </div>
                                    <div class="input-group">
                                          <label for="email">Plan</label>  
                                          <select name="plan" class="form-control required has-success" placeholder="Please select">
                                                <option value="650">1 month Plan</option>
                                                <option value="1755">3 month Plan</option>
                                                <option value="3315">6 month Plan</option>
                                          </select>
                                    </div>
                                    <br/>
                                    <div class="form-group">
                                      <label for="email">Email address:</label>
                                      <input type="email" value="<?php echo $res[$i]['email'];?>" name="email" class="form-control" id="email">
                                    </div>
                                    <br/>
                                    <div class="input-group">
                                        <button type="submit" class="btn btn-default">Submit</button>
                                    </div>
                                </form>
                              </div>
                              <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </div>
                          </div>
      
                    </div>
                </div>
                  <!----------------------------------End model for pop up form---------------------------->
                        <?php  }?>    
                    </tbody>
                  </table>
                    <!--<div class="box"><?php //include('paging.php');?></div>-->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
		  
        </section><!-- /.content -->
		
      </div><!-- /.content-wrapper --><!-- /.content -->
      <link rel="stylesheet" type="text/css" href=" https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css">
 
      <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
   <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script> 

  <script>   
  $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<?php }?>


<!------------------------edit Section start---------------------------->
<?php if($a=='edit')
{ 
	$id=loadvariable('id','');	
	$SQL="select users.*,members.* FROM `users` inner join members on members.id = users.member_id WHERE users.member_id='".$id."'";
	
	$res=$db->get_results($SQL);
	
	?>


							  
				  <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Update Details For Users Infomaition </h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="post" action="../model/users.php">
				<input type="hidden" name="a" value="edit">
				<input type="hidden" name="id" value="<?php echo $res[0]['member_id']; ?>">
                  <div class="box-body">
                    
					<div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">First Name</label>
                      <div class="col-sm-6">
                        <input type="text"  value="<?php echo $res[0]['first_name']?>" class="form-control" name="first_name" id="inputEmail3" placeholder="First Name ">
                      </div>
                    </div>
                   
                   <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Last Name</label>
                      <div class="col-sm-6">
                        <input type="text"  value="<?php echo $res[0]['last_name']?>" class="form-control" name="last_name" id="inputEmail3" placeholder="Last Name ">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Sex</label>
                      <div class="col-sm-6">
                      	<select class="form-control" name="sex" id="dptcentres_edit" placeholder="sex" >
                      		<option  value="<?= $res[0]['sex'];?>" selected disabled><?= $res[0]['sex'];?></option>
                      		<option  value="Male">Male</option>
                      		<option value="Female">Female</option>	
						</select>
						</div>
					</div>
                   <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                      <div class="col-sm-6">
                        <input type="text"  value="<?php echo $res[0]['email'] ?>"class="form-control" name="email" id="inputEmail3" placeholder="Email ">
                      </div>
                    </div>
                   
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Username</label>
                      <div class="col-sm-6">
                        <input type="text"  value="<?php echo $res[0]['username'] ?>"class="form-control" name="username" id="inputEmail3" placeholder="username ">
                      </div>
                    </div>
                   
                   <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Mobile No</label>
                      <div class="col-sm-6">
                        <input type="text"  value="<?php echo $res[0]['phone_number'] ?>"class="form-control" name="phone" id="inputEmail3" placeholder="Mobile No ">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Marital Status</label>
                      <div class="col-sm-6">
                      	<select class="form-control" name="marital_status">
                      		<option value="<?php  echo get_marital_status($res[0]['marital_status']); ?>"><?php echo get_marital_status($res[0]['marital_status']); ?></option>
                      		<option value="1">Single</option>
                      		<option value="2">Seperated</option>
                      		<option value="3">Divorced</option>
                      		<option value="4">Widowed</option>
                      		<option value="5">Annulled</option>
                      		
						</select>
						</div>
					</div>	

					<div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label" >Birthday</label>
                      <div class="col-sm-6">
                        <input type="text"  value="<?php echo $res[0]['birthday'];  ?>"class="form-control" name="birthday" id="inputEmail3" placeholder="Age" data-provide="datepicker" data-date-format="yyyy-mm-dd">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label" >City</label>
                      <div class="col-sm-6">
                        <input type="text"  value="<?php echo $res[0]['city'];  ?>"class="form-control" name="city" id="inputEmail3" placeholder="City" >
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label" >State</label>
                      <div class="col-sm-6">
                        <input type="text"  value="<?php echo $res[0]['state'];  ?>"class="form-control" name="state" id="inputEmail3" placeholder="State">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label" >Country</label>
                      <div class="col-sm-6">
                        <input type="text"  value="<?php echo $res[0]['country'];  ?>"class="form-control" name="country" id="inputEmail3" placeholder="Country">
                      </div>
                    </div>
                   
                   
                   
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Education</label>
                      <div class="col-sm-6">
                        <input type="text"  value="<?php echo get_education($res[0]['education']); ?>"class="form-control" name="education" id="inputEmail3" placeholder="education">
                      </div>
                    </div>
                   
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Profession</label>
                      <div class="col-sm-6">
                        <input type="text"  value="<?php echo $res[0]['profession'] ?>"class="form-control" name="employment" id="inputEmail3" placeholder="Profession">
                      </div>
                    </div>
                   
                   
                    
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">About</label>
                      <div class="col-sm-6">
                        <textarea rows="4" cols="50" class="form-control" name="about" id="inputEmail3" placeholder="about"><?php echo $res[0]['about_me']; ?></textarea>
                      </div>
                    </div>	

					<div class="form-group">
                    	<button type="submit" class="btn btn-default">Cancel</button>
                    	<button type="submit" class="btn btn-info" id="btnSubmit">update</button>
                  	</div>
				</div><!-- /.box-body -->
                </div>  <!-- /.box-footer -->
                </form>
               <?php }?>
				  
              </div>
<!------------------------edit Section End----------------------------> 
<script>
var temp = "php";

// Create New Option.
var newOption = $('<option>');
newOption.attr('value', temp).text(temp);

// Append that to the DropDownList.
$('#dptcentres_edit').append(newOption);

// Select the Option.
$("#dptcentres_edit > [value=" + temp + "]").attr("selected", "true");
              
              </script>