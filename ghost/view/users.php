<?php 

$db=new DB();
$str="";
$start=loadvariable('start','0');
$end=30;
$msg=loadvariable('msg','');
$action=loadvariable('action','');
$a=loadvariable('a','list');
if($msg=='1')
{
		$str="Successfully Updated ";
		$cls="alert alert-success";
}
if($msg=='0')
{
		$str="Not Updated ....Problem Occured ";
		$cls="alert alert-danger";
}
if($action=='search')
{
$val=loadvariable('val','');
$option=loadvariable('option','');
}
if($msg==10)
{
$str="Successfully waived user for 1 month ";
$cls="alert alert-success";

}


$SQL="select members.*, users.* from users inner join members on users.member_id=members.id";
if($val!="")
{
	if($option==0)
	{
	$SQL.=" WHERE members.first_name LIKE '%$val%'";
	}
	if($option==1)
	{
	$SQL.=" WHERE members.phone_number='$val'";
	}
	if($option==2)
	{  
	$SQL.=" WHERE users.email='$val'";
	}
}
$SQL.=" ORDER BY  users.id desc LIMIT $start,$end";
$res=$db->get_results($SQL);

$SQL_COUTN="select members.*, users.* from users inner join members on users.member_id=members.id";
if($val!="")
{
	if($option==0)
	{
	$SQL_COUTN.=" WHERE members.first_name LIKE '%$val%'";
	}
	if($option==1)
	{
	$SQL_COUTN.=" WHERE members.phone_number='$val'";
	}
	if($option==2)
	{
	$SQL_COUTN.=" WHERE users.email='$val'";
	}
}
$results1=$db->num_rows($SQL_COUTN);
$thispage=$_SERVER['PHP_SELF']."?p=users";
$num=$results1;
$per_page=30;
$showeachside=5;

if(empty($start)){$start=0;}
$max_pages=  ceil($num/$per_page);
$cur=  ceil($start/$per_page)+1; 


?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
	  <section class="content">
	   <?php if($a=='list')
			{ ?>
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>Users Management</h1>
          <ol class="breadcrumb">
	  <li><a href="index.php"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="index.php?p=users">Users</a></li> 	
          </ol>
		  <?php// echo  $SQL;?>
        </section>
      <div class="content">
         <!-- Main content -->
                <?php 
				if($str!="")
				{?>
				<div class="<?php echo  $cls;?>" role="alert">
			  <a href="#" class="alert-link"><?php  echo $str;?></a>
			</div>
			<?php }
			?> 
          <div class="row">
            <div class="content">
      <!----------------------start search box---->
              <div class="box">
                 <div class="col-lg-12">
							<form class="navbar-form  pull-right" action="index.php"  role="search">
							<input type="hidden" name="p" value="users">
							<input type="hidden" name="action" value="search">
                <div class="input-group">
                    <input type="text" class="form-control" size="20" placeholder="Search" name="val">
                </div>
				 <div class="input-group">
                    <select class="form-control" name="option">
					<option value="0">Name </option>
					<option value="1">Mobile </option>
					<option value="2">Email </option>
					</select>
                </div>
			    <div class="input-group">
                    <input type="submit" class="form-control btn btn-primary" value="Search">
                </div>
				</form>
				         </div>
                       </div>
                     <!----------end search------------------------>
					 
                <div class="box-body">
                  <table id="example" style="font-size:14px" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th style="text-align: center;">Sr. no.</th>
                        <th style="text-align: center;">Name</th>
                        <th style="text-align: center;">Sex</th>
                        <th style="text-align: center;">Age</th>
                        <th style="text-align: center;">Mobile no.</th>
						<th style="text-align: center;">Payment expires</th>
						<th style="text-align: center;">Email</th>
                        <th style="text-align: center;">Address</th>
                        <!--<th style="text-align: center;">Religion</th>-->
                        <th style="text-align: center;">Status</th>
						<th style="text-align: center;">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php  $date1=date('Y-m-d H:i:s');
						for($i=0;$i<count($res);$i++)
                        { ?>
						<tr>
                                <td style="text-align: center;"><?php echo $start+$i+1; ?></td>
                                <td style="text-align: center;"><?php echo  $res[$i]['first_name']." ".$res[$i]['last_name'] ?></td>
                                <td style="text-align: center;"><?php echo ucwords($res[$i]['sex']);?></td>
                                <td style="text-align: center;"><?php member_age($res[$i]['birthday']);?></td>
                                <td style="text-align: center;"> <?php echo $res[$i]['phone_number'];?></td>
								
								<td style="text-align: center;"> <?php
								$date2=date("Y-m-d", strtotime($res[$i]['payment_expires']));
								
								if(strtotime($date1) > strtotime($date2))
								{  echo  "<br>".$date2;?>
									<a class='btn btn-danger btn-sm' href='../model/users.php?id=<?php echo $res[$i]['id'];?>&a=updateexp&start=<?php echo $start;?>&action=<?php echo $action;?>&val=<?php echo $val;?>&option=<?php echo $option; ?>' >Waive Off Expired</a>
								<?php } else 
								{?>
								<a class='btn btn-success btn-sm' href='../model/users.php?id=<?php echo $res[$i]['id'];?>&a=updateexp&start=<?php echo $start;?>&action=<?php echo $action;?>&val=<?php echo $val;?>&option=<?php echo $option; ?>' >Waived Off </a>
									<?php echo  "<br>".$date2;
								}?>
								
								</td>
								
								
								<td style="text-align: center;"> <?php echo $res[$i]['email'];?></td>
                 <td style="text-align: center;"><?php echo  $res[$i]['city']." <br>".$res[$i]['district']."<br>".$res[$i]['state']."<br> ".$res[$i]['country']; ?></td> 
                               <!-- <td style="text-align: center;"><?php  religion($res[$i]['religion']);echo $reli[$i]['Value']?> </td>-->
                             
						<td> 
					   <?php if($res[$i]['is_active']=='1')
								{?>
									<a class="btn btn-success" style="padding:2px;" href="../model/users.php?a=update&start=<?php echo $start;?>&status=0&id=<?php echo $res[$i]['id']; ?>&action=<?php echo $action;?>&val=<?php echo $val;?>&option=<?php echo $option; ?>" title="Click to IN-Active Mode"><i class="fa fa-check"></i></a>
								<?php }?>
								<?php
								if($res[$i]['is_active']=='0')	
								{?>
									<a class="btn btn-danger" style="padding:2px;" href="../model/users.php?a=update&start=<?php echo $start;?>&status=1&id=<?php echo $res[$i]['id'];?>&action=<?php echo $action;?>&val=<?php echo $val;?>&option=<?php echo $option; ?>" title="Click to Active Mode"><i class="fa fa-hand-pointer-o"></i></a>
								<?php }?>
						</td>
						</td>
					   <td>
					   <a class="btn btn-success" style="padding:2px;" href="index.php?p=users&a=edit&id=<?php echo $res[$i]['member_id']; ?>"><i class="fa fa-edit"></i> &nbsp; Edit</a>
					 <br>
					 <br>
							<?php if($res[$i]['is_blocked']=='1')
								{?>
									<a class="btn btn-danger btn-sm" style="padding:2px;" href="../model/users.php?a=block&start=<?php echo $start;?>&is_blocked=0&id=<?php echo $res[$i]['id']; ?>&action=<?php echo $action;?>&val=<?php echo $val;?>&option=<?php echo $option; ?>" title="Click to IN-Active Mode"><i class="fa fa-remove"></i>&nbsp; Unblock</a>
								<?php }?>
								<?php
								if($res[$i]['is_blocked']=='0')	
								{?>
									<a class="btn btn-info btn-sm" style="padding:2px;" href="../model/users.php?a=block&start=<?php echo $start;?>&is_blocked=1&id=<?php echo $res[$i]['id'];?>&action=<?php echo $action;?>&val=<?php echo $val;?>&option=<?php echo $option; ?>" title="Click to Active Mode"><i class="fa fa-hand-pointer-o"></i> &nbsp;Block</a>
								<?php }?>
					   
					   </td>
                      </tr>
                        <?php  }?>
                    </tbody>
                   
                  </table>
				  
                    <div class="box"><?php include('paging.php');?></div>
				
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
		  
        </section><!-- /.content -->
		
      </div><!-- /.content-wrapper --><!-- /.content -->
      <link rel="stylesheet" type="text/css" href=" https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css">
 
      <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
   <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script> 

  <script>   
  $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<?php }?>
<?php if($a=='edit')
{ 
	$id=loadvariable('id','');	
	$SQL="select users.*,members.* FROM `users` inner join members on members.id = users.member_id WHERE users.member_id='".$id."'";
	
	$res=$db->get_results($SQL);
	
	?>


							  
				  <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Update Details For Users Infomaition </h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="post" action="../model/users.php">
				<input type="hidden" name="a" value="edit">
				<input type="hidden" name="id" value="<?php echo $res[0]['member_id']; ?>">
                  <div class="box-body">
                    
					<div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">First Name</label>
                      <div class="col-sm-6">
                        <input type="text"  value="<?php echo $res[0]['first_name']?>" class="form-control" name="first_name" id="inputEmail3" placeholder="First Name ">
                      </div>
                    </div>
                   
                   <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Last Name</label>
                      <div class="col-sm-6">
                        <input type="text"  value="<?php echo $res[0]['last_name']?>" class="form-control" name="last_name" id="inputEmail3" placeholder="Last Name ">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Sex</label>
                      <div class="col-sm-6">
                        <select class="form-control" name="sex" id="dptcentres_edit" value="<?= $res[0]['sex'];?>" placeholder="sex" >
                          <option <?php if($res[0]['sex'] == 'Male' || $res[0]['sex'] == 'male') { echo 'selected';}?> value="Male">Male</option>
                          <option <?php if($res[0]['sex'] == 'Female' || $res[0]['sex'] == 'female') { echo 'selected';}?> value="Female">Female</option>
                        </select>
						</div>
					</div>
                   <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                      <div class="col-sm-6">
                        <input type="text"  value="<?php echo $res[0]['email'] ?>"class="form-control" name="email" id="inputEmail3" placeholder="Email ">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Username</label>
                      <div class="col-sm-6">
                        <input type="text"  value="<?php echo $res[0]['username'] ?>"class="form-control" name="username" id="inputEmail3" placeholder="username ">
                      </div>
                    </div>
                   
                   <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Mobile no.</label>
                      <div class="col-sm-6">
                        <input type="text"  value="<?php echo $res[0]['phone_number'] ?>"class="form-control" name="phone" id="inputEmail3" placeholder="Mobile No ">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Marital Status</label>
                      <div class="col-sm-6">
                      	<select class="form-control" name="marital_status">
                      		<option <?php if($res[0]['marital_status'] ==1) {echo "selected" ;}?>value="1">Single</option>
                      		<option <?php if($res[0]['marital_status'] ==2) {echo "selected" ;}?> value="2">Seperated</option>
                      		<option <?php if($res[0]['marital_status'] ==3) {echo "selected" ;}?> value="3">Divorced</option>
                      		<option <?php if($res[0]['marital_status'] ==4) {echo "selected" ;}?> value="4">Widowed</option>
                      		<option <?php if($res[0]['marital_status'] ==5) {echo "selected" ;}?> value="5">Annulled</option>
                      		
						</select>
						</div>
					</div>	

					<div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label" >Birthday</label>
                      <div class="col-sm-6">
                        <input type="text"  value="<?php echo $res[0]['birthday'];  ?>"class="form-control" name="birthday" id="inputEmail3" placeholder="Age" data-provide="datepicker" data-date-format="yyyy-mm-dd">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label" >City</label>
                      <div class="col-sm-6">
                        <input type="text"  value="<?php echo $res[0]['city'];  ?>"class="form-control" name="city" id="inputEmail3" placeholder="City" >
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label" >State</label>
                      <div class="col-sm-6">
                        <input type="text"  value="<?php echo $res[0]['state'];  ?>"class="form-control" name="state" id="inputEmail3" placeholder="State">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label" >Country</label>
                      <div class="col-sm-6">
                        <input type="text"  value="<?php echo $res[0]['country'];  ?>"class="form-control" name="country" id="inputEmail3" placeholder="Country">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Education</label>
                      <div class="col-sm-6">
                          <select value="<?php echo get_education($res[0]['education']); ?>" class="form-control" name="education" id="inputEmail3" placeholder="education"> 
                              <option <?php if($res[0]['education'] = 1){echo "selected" ;}?> value="1">Below High School</option>
                              <option <?php if($res[0]['education'] = 2){echo "selected" ; }?> value="2">High School</option>
                              <option <?php if($res[0]['education'] = 3){echo "selected" ;}?> value="3">Bachelors</option>
                              <option <?php if($res[0]['education'] = 4){echo "selected" ;}?> value="4">Masters</option>
                              <option <?php if($res[0]['education'] = 5){echo "selected" ;}?> value="5">Doctorate</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Profession</label>
                      <div class="col-sm-6">
                         <select name="employment"class="form-control input-xlarge required" data-placeholder="Please Select">
                              <option <?php if($res[0]['profession'] == 'Architect'){echo "selected";} ?> value="Architect">Architect</option>
                              <option <?php if($res[0]['profession'] == 'Army'){echo "selected";} ?> value="Army">Army</option>
                              <option <?php if($res[0]['profession'] == 'Bank Officer'){echo "selected";} ?> value="Bank Officer">Bank Officer</option>
                              <option <?php if($res[0]['profession'] == 'Businessperson'){echo "selected";} ?> value="Businessperson">BusinessMan</option>
                              <option <?php if($res[0]['profession'] == 'Clerk'){echo "selected";} ?> value="Clerk">Clerk</option>
                              <option <?php if($res[0]['profession'] == 'Doctor'){echo "selected";} ?> value="Doctor">Doctor</option>
                              <option <?php if($res[0]['profession'] == 'Designer'){echo "selected";} ?> value="Designer">Designer</option>
                              <option <?php if($res[0]['profession'] == 'Engineer'){echo "selected";} ?> value="Engineer">Engineer</option>
                              <option <?php if($res[0]['profession'] == 'Farmer'){echo "selected";} ?> value="Farmer">Farmer</option>
                              <option <?php if($res[0]['profession'] == 'Government Officer'){echo "selected";} ?> value="Government Officer">Government Officer</option>
                              <option <?php if($res[0]['profession'] == 'Lawyer'){echo "selected";} ?> value="Lawyer">Lawyer</option>
                              <option <?php if($res[0]['profession'] == 'Lecturer'){echo "selected";} ?> value="Lecturer">Lecturer</option>
                              <option <?php if($res[0]['profession'] == 'Nurse'){echo "selected";} ?> value="Nurse">Nurse</option>
                              <option <?php if($res[0]['profession'] == 'Teacher'){echo "selected";} ?> value="Teacher">Teacher</option>
                              <option <?php if($res[0]['profession'] == 'Programmer'){echo "selected";} ?> value="Programmer">Programmer</option>
                              <option <?php if($res[0]['profession'] == 'Politician'){echo "selected";} ?> value="Politician">Politician</option>
                              <option <?php if($res[0]['profession'] == 'Police Officer'){echo "selected";} ?> value="Police Officer">Police Officer</option>
                              <option <?php if($res[0]['profession'] == 'Scientist'){echo "selected";} ?> value="Scientist">Scientist</option>
                              <option <?php if($res[0]['profession'] == 'Student'){echo "selected";} ?> value="Student">Student</option>
                              <option <?php if($res[0]['profession'] == 'Servicemen'){echo "selected";} ?> value="Servicemen">Servicemen</option>
                              <option <?php if($res[0]['profession'] == 'Secretary'){echo "selected";} ?> value="Secretary">Secretary</option>
                              <option <?php if($res[0]['profession'] == 'Social Worker'){echo "selected";} ?> value="Social Worker">Social Worker</option>
                              <option <?php if($res[0]['profession'] == 'Sportsmen'){echo "selected";} ?> value="Sportsmen">Sportsmen</option>
                              <option <?php if($res[0]['profession'] == 'Other'){echo "selected";} ?> value="Other">Other</option>
                       </select>
           
                      </div>
                    </div>
                   
                   
                    
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">About</label>
                      <div class="col-sm-6">
                        <textarea rows="4" cols="50" class="form-control" name="about" id="inputEmail3" placeholder="about"><?php echo $res[0]['about_me']; ?></textarea>
                      </div>
                    </div>	

					<div class="form-group">
                    	<button type="submit" class="btn btn-default">Cancel</button>
                    	<button type="submit" class="btn btn-info" id="btnSubmit">update</button>
                  	</div>
				</div><!-- /.box-body -->
                </div>  <!-- /.box-footer -->
                </form>
               <?php }?>
				  
              </div>
              
<script>
var temp = "php";

// Create New Option.
var newOption = $('<option>');
newOption.attr('value', temp).text(temp);

// Append that to the DropDownList.
$('#dptcentres_edit').append(newOption);

// Select the Option.
$("#dptcentres_edit > [value=" + temp + "]").attr("selected", "true");
              
              </script>