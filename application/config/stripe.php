<?php defined('SYSPATH') or die('No direct script access.');

return array(
         'secret_key' => 'sk_live_FPkdOchthAILPt3utOGTMRiZ',  //live keys from Stripe
         'publishable_key' => 'pk_live_VUksTIdpymZyjOwTWkHpC3fi',
		/**************for dollor usd*********************/		 
         'monthly_charge'  => 999, //cents
         '3monthly_charge' => 2699,
         '6monthly_charge' => 5099,
         'feature_charge'  => 7999,
         'monthly_charge_ds_for_offer' => 499,
         '3monthly_charge_ds_for_offer' => 1349,
         '6monthly_charge_ds_for_offer' => 4249,
         'feature_charge_ds_for_offer' => 3999,
		 /************rs vaslue*********/
         'feature_charge_rs' => 520000,
         'monthly_charge_rs' => 65000,
         '3monthly_charge_rs' => 175500,
         '6monthly_charge_rs' => 331800,
         /*'6monthly_charge_rs' => 330000,*/
         'feature_charge_rs_for_offer' => 260000,
         'monthly_charge_rs_for_offer' => 32500,
         '3monthly_charge_rs_for_offer' => 87750,
         '6monthly_charge_rs_for_offer' => 165750,
        /***************************for view only ******************/
         'monthly_charge_dollar' =>  '$9.99',
         'monthly_charge_dollar_for_offer' =>  '$4.99',
         '3monthly_charge_dollar' => '$26.99',
         '3monthly_charge_dollar_for_offer' => '$13.49',
         '6monthly_charge_dollar' => '$50.99',
         '6monthly_charge_dollar_for_offer' => '$42.49',
         'feature_charge_dollar' =>  '$79.99',
         'feature_charge_dollar_for_offer' =>  '$39.99',
         'feature_charge_rupees' =>  '₹5199',
         'feature_charge_rupees_for_offer' =>  '₹2599.5',
         'monthly_charge_rupees' =>  '₹650 ',
         'monthly_charge_rupees_for_offer' =>  '₹325',
         '3monthly_charge_rupees' => '₹1755',
         '3monthly_charge_rupees_for_offer' => '₹877.5',
         '6monthly_charge_rupees' => '₹3318',
         /*'6monthly_charge_rupees' => '₹3300',*/
         '6monthly_charge_rupees_for_offer' => '₹1657.5',

      
);