<?php

defined('SYSPATH') or die('No direct script access.');

// -- Environment setup --------------------------------------------------------
// Load the core Kohana class
require SYSPATH . 'classes/kohana/core' . EXT;

if (is_file(APPPATH . 'classes/kohana' . EXT)) {
    // Application extends the core
    require APPPATH . 'classes/kohana' . EXT;
} else {
    // Load empty core extension
    require SYSPATH . 'classes/kohana' . EXT;
}

/**
 * Set the default time zone.
 *
 * @link http://kohanaframework.org/guide/using.configuration
 * @link http://www.php.net/manual/timezones
 */
date_default_timezone_set('America/Chicago');

/**
 * Set the default locale.
 *
 * @link http://kohanaframework.org/guide/using.configuration
 * @link http://www.php.net/manual/function.setlocale
 */
setlocale(LC_ALL, 'en_US.utf-8');

/**
 * Enable the Kohana auto-loader.
 *
 * @link http://kohanaframework.org/guide/using.autoloading
 * @link http://www.php.net/manual/function.spl-autoload-register
 */
spl_autoload_register(array('Kohana', 'auto_load'));

/**
 * Enable the Kohana auto-loader for unserialization.
 *
 * @link http://www.php.net/manual/function.spl-autoload-call
 * @link http://www.php.net/manual/var.configuration#unserialize-callback-func
 */
ini_set('unserialize_callback_func', 'spl_autoload_call');

// -- Configuration and initialization -----------------------------------------

/**
 * Set the default language
 */
I18n::lang('en-us');
Cookie::$salt = 'Your-Salt-Goes-Here';

/**
 * Set Kohana::$environment if a 'KOHANA_ENV' environment variable has been supplied.
 *
 * Note: If you supply an invalid environment name, a PHP warning will be thrown
 * saying "Couldn't find constant Kohana::<INVALID_ENV_NAME>"
 */
if (isset($_SERVER['KOHANA_ENV'])) {
    Kohana::$environment = constant('Kohana::' . strtoupper($_SERVER['KOHANA_ENV']));
}

/**
 * Initialize Kohana, setting the default options.
 *
 * The following options are available:
 *
 * - string   base_url    path, and optionally domain, of your application   NULL
 * - string   index_file  name of your index file, usually "index.php"       index.php
 * - string   charset     internal character set used for input and output   utf-8
 * - string   cache_dir   set the internal cache directory                   APPPATH/cache
 * - integer  cache_life  lifetime, in seconds, of items cached              60
 * - boolean  errors      enable or disable error handling                   TRUE
 * - boolean  profile     enable or disable internal profiling               TRUE
 * - boolean  caching     enable or disable internal caching                 FALSE
 * - boolean  expose      set the X-Powered-By header                        FALSE
 */
Kohana::init(array(
    'base_url' => 'https://www.maangu.com/',
    'index_file' => false,
	 'errors' => TRUE,
));

/**
 * Attach the file write to logging. Multiple writers are supported.
 */
Kohana::$log->attach(new Log_File(APPPATH . 'logs'));

/**
 * Attach a file reader to config. Multiple readers are supported.
 */
Kohana::$config->attach(new Config_File);

/**
 * Enable modules. Modules are referenced by a relative or absolute path.
 */
Kohana::modules(array(
    'auth' => MODPATH . 'auth', // Basic authentication
    // 'cache'      => MODPATH.'cache',      // Caching with multiple backends
    // 'codebench'  => MODPATH.'codebench',  // Benchmarking tool
    'database' => MODPATH . 'database', // Database access
    'image' => MODPATH . 'image', // Image manipulation
    'orm' => MODPATH . 'orm', // Object Relationship Mapping
    // 'unittest'   => MODPATH.'unittest',   // Unit testing
    'userguide' => MODPATH . 'userguide', // User guide and API documentation
    'email' => MODPATH . 'email', // User guide and API documentation
));

/**
 * Set the routes. Each route must have a minimum of a name, a URI and a set of
 * defaults for the URI.
 */
if (empty($_SERVER['HTTP_HOST'])) {

    Route::set('default', '(<controller>(/<action>(/<id>(/<page>))))')
        ->defaults(array(
                'controller' => 'cron',
                'action' => 'index',
            )
        );
} else if (!Auth::instance()->logged_in()) {

    Route::set('public-search', 'search')
        ->defaults(array(
                'controller' => 'pages',
                'action' => 'advance_search',
            )
        );

    Route::set('public-searchresult', 'searchresult')
        ->defaults(array(
                'controller' => 'pages',
                'action' => 'searchresult',
            )
        );

    Route::set('public-login', 'login')
        ->defaults(array(
                'controller' => 'pages',
                'action' => 'login',
            )
        );

    Route::set('default', '(<controller>(/<action>(/<id>(/<page>))))', array('controller' => '(report|pages|company|register|members)'))
        ->defaults(array(
                'controller' => 'pages',
                'action' => 'index',
            )
        );

    Route::set('staticpages', '<action>', array('action' => '(divorcedmatrimony|indiansingles|matrimony|matrimonials|sitemap|support|pricing|report|login|signup|advertise)'))
        ->defaults(array(
                'controller' => 'pages',
            )
        );
     
    Route::set('forgot-password', 'forgot-password')
        ->defaults(array(
                'controller' => 'pages',
                'action' => 'forgot_password',
            )
        );

    Route::set('activation-link', 'activation-link')
        ->defaults(array(
                'controller' => 'pages',
                'action' => 'resend_link',
            )
        );

    Route::set('matrimonypages', 'matrimony/<page>')
        ->defaults(array(
                'controller' => 'pages',
                'action' => 'matrimony',
            )
        );

		Route::set('indiansinglespages', 'indiansingles/<page>')
        ->defaults(array(
                'controller' => 'pages',
                'action' => 'indiansingles',
            )
        );
      Route::set('divorcedmatrimonypages', 'divorcedmatrimony/<page>')
        ->defaults(array(
                'controller' => 'pages',
                'action' => 'divorcedmatrimony',
            )
        );
    Route::set('matrimonialpages', 'matrimonials/<page>')
        ->defaults(array(
                'controller' => 'pages',
                'action' => 'matrimonials',
            )
        );

    Route::set('username', '<username>')
        ->defaults(array(
                'controller' => 'pages',
                'action' => 'profile',
            )
        );
			Route::set('error', 'error/<action>/(<message>)', array('action' => '[0-9]++', 'message' => '.+'))
				->defaults(array(
					'controller' => 'error',
					'action'     => '404'
			));

//    Route::set('search', '<search>', array('search' => '(search)'))
//        ->defaults(array(
//                'controller' => 'pages',
//                'action' => 'advance_search',
//            )
//        );
} else {

    Route::set('main', '(<controller>(/<action>(/<id>(/<page>))))', array('controller' => '(members|profile|admin|pages|messages|register)'))
        ->defaults(array(
                'controller' => 'members',
                'action' => 'index',
            )
        );

		Route::set('staticpages', '<action>', array('action' => '(divorcedmatrimony|indiansingles|matrimony|matrimonials|sitemap|support|pricing|report|login|signup|advertise)'))
        ->defaults(array(
                'controller' => 'pages',
            )
        );


        Route::set('matrimonypages', 'matrimony/<page>')
        ->defaults(array(
                'controller' => 'pages',
                'action' => 'matrimony',
            )
        );

        Route::set('indiansinglespages', 'indiansingles/<page>')
        ->defaults(array(
                'controller' => 'pages',
                'action' => 'indiansingles',
            )
        );
      Route::set('divorcedmatrimonypages', 'divorcedmatrimony/<page>')
        ->defaults(array(
                'controller' => 'pages',
                'action' => 'divorcedmatrimony',
            )
        );
    Route::set('matrimonialpages', 'matrimonials/<page>')
        ->defaults(array(
                'controller' => 'pages',
                'action' => 'matrimonials',
            )
        );
                    




    Route::set('search', '<search>', array('search' => '(search)'))
        ->defaults(array(
                'controller' => 'pages',
                'action' => 'advance_search',
            )
        );

    Route::set('settings', 'settings/<action>', array('action' => '(profile|partner|account|password|subscription|email|username)'))
        ->defaults(array(
                'controller' => 'profile',
            )
        );

    Route::set('profile', '<username>')
        ->defaults(array(
                'controller' => 'members',
                'action' => 'profile',
            )
        );

    Route::set('photos', '<username>/<page>', array('page' => '(photos|info|followers|following|partner_info)'))
        ->defaults(array(
                'controller' => 'members',
                'action' => 'profile_pages',
            )
        );
		Route::set('ask', '<username>/<action>', array('action' => '(askphoto|ask_profile_info)'))
            ->defaults(array(
                'controller' => 'members',
                    )
    );

    Route::set('default', '(<controller>(/<action>(/<id>(/<page>))))')
        ->defaults(array(
                'controller' => 'members',
                'action' => 'index',
            )
        );
		Route::set('error', 'error(/<action>(/<message>))', array('action' => '[0-9]++', 'message' => '.+'))
       ->defaults(array(
						'controller' => 'error',
						'action' => '404',
						'message' => 'Not found'
     ));
}