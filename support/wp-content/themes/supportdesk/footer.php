<!-- #footer-bottom -->
<footer id="footer">
   
    <div id="footer-bottom" class="clearfix">
       <div class="row"> 
           <nav id="footer-nav" role="navigation"> 
                   <div class="row">
            <ul class="list-unstyled footer-nav">
                <li>
                    <a href="https://www.maangu.com/"><i class="fa fa-home"></i> Home</a>
                </li>
                <li>
                    <a href="https://www.maangu.com/company/about"><i class="fa fa-info-circle"></i> About</a>
                </li>
                <li>
                    <a href="https://www.maangu.com/pricing"><i class="fa fa-dollar"></i> Pricing</a>
                </li>
                <li>
                    <a href="https://www.maangu.com/blog"><i class="fa fa-comments"></i> Blog</a>
                </li>
				<li>
                    <a href="https://www.maangu.com/company/press"><i class="fa fa-bullhorn"></i> Press</a>
                </li>
                <li>
                    <a href="https://www.maangu.com/company/csr"><i class="fa fa-users"></i> CSR</a>
                </li>
                <li>
                    <a href="https://www.maangu.com/company/privacy"><i class="fa fa-lock"></i> Privacy</a>
                </li>
                <li>
                    <a href="https://www.maangu.com/company/terms"><i class="fa fa-file"></i> Terms</a>
                </li>
                <li>
                    <a href="https://www.maangu.com/support"><i class="fa fa-wrench"></i> Support</a>
                </li>
                <li>
                    <a href="https://www.maangu.com/report/successstory"><i class="fa fa-thumbs-down"></i> Report</a>
                </li>
                <li>
                    <a href="https://www.maangu.com/company/contact"><i class="fa fa-location-arrow"></i> Contact</a>
                </li>
            </ul> 
            </nav> </div>
               <div class="row">
            <div class="footer-social">
                <ul class="list-unstyled list-inline footer-social-link" style="padding-left: 45%">
                    <li><a href="https://www.facebook.com/maangudotcom"><img src="<?php bloginfo('template_url'); ?>/images/facebook.png" alt="Maangu-facebook"></a></li>
                    <li><a href="https://twitter.com/maangucom"><img src="<?php bloginfo('template_url'); ?>/images/twitter.png" alt="maangu-com-twitter"></a></li>
                    <li><a href="https://www.linkedin.com/company/maangu"><img src="<?php bloginfo('template_url'); ?>/images/linkedin.png" alt="maangu-com-linkedin"></a></li>
                </ul>
            <div>
            <!-- /.footer-social -->
            <div class="row footer-copyright" style="text-align: center;font-size: 12px">
                 &copymaangu 2015. All rights reserved.</div>
             
        </div>
       
        <!-- /.row -->
      
           
    </div>
</footer> <!-- /#footer-bottom -->
    <?php wp_footer(); ?>
</body>
</html>