$(document).ready(function() {
    var base_url = $('#base_url').val();
    
     $('.navbar-toggler').on('click', function(event) {
        event.preventDefault();
        $(this).closest('.navbar-minimal').toggleClass('open');
    })
    
    $('.selectpicker').selectpicker();
    
    if($(".select2me").length > 0) {
        $(".select2me").select2();
    }
    
    $('.uploadDPBtn').click(function(event) {
        $(this).closest('form').find('#dp-input').trigger('click');
        event.preventDefault();
    });
    
    $('.un-suggestion').change(function(){
        $('#selected-username').val($('.un-suggestion:checked').val());
    });
    
    $('body').on('submit', '.search-by-username', function() {
        var elem = $(this);
        elem.find('p.search-msg').hide();

        if($.trim(elem.find('input').val()) == '') {
            return false;
        }

        elem.find('img').closest('div').show();
        
        elem.ajaxSubmit({
            success:function(data) {
                data = $.parseJSON(data);
                
                if(data.id) {
                     window.location.href = base_url+data.id;
                } else {
                    elem.find('img').closest('div').hide();
                    elem.find('p.search-msg').show();
                }
            }
        });
        
        return false;
    });
    
    $('#footerLogin').on('submit', '.refer-form', function() {
        var elem = $(this);
        elem.find('.alert').hide();

        elem.find('img').closest('div').show();
        
        elem.ajaxSubmit({
            success:function(data) {
                data = $.parseJSON(data);
                
                if(data.status == 'success') {
                    elem.find('#refer-success-text').html(data.msg);
                    elem.find('.alert-success').show();
                    
                    elem.find('#refer-form-content').hide();
                } else {
                    elem.find('#refer-error-text').html(data.msg);
                    elem.find('.alert-danger').show();
                }
                
                elem.find('img').closest('div').hide();
            }
        });
        
        return false;
    });

    $('#dp-input').change(function() {
        $(this).closest('form').submit();
    });
    
    $('.dp-form').submit(function() {
        var element = $(this);
        $('#user-dp').find('.img-div').addClass('loading');
        $('#user-dp').children('.image-loader').show();

        $(this).ajaxSubmit({
            success:function(data) {
                data = $.parseJSON(data);

                $('#editImageModal .div-to-edit').html('');
                
                $('#editImageModal .modal-dialog').css('width', data.width+'px');
                $('#editImageModal .modal-dialog').css('height', data.height+'px');
                $('#editImageModal .modal-header').hide();
                $('#editImageModal .div-to-edit').html($('<img />').attr({ id: 'selectArea', 'src': base_url+'upload/'+data.image }));
                $('input[name="imag_name"]').val(data.image);

                $('#editImageModal').modal({
                    show:true,
                    backdrop:"static",
                    keyboard: false,
                });

                $('#editImageModal').on('shown.bs.modal', function () {
                    $('#selectArea').imgAreaSelect({ 
                        aspectRatio: '9:8',
                        handles: true,
                        parent: '#editImageModal',
                        x1: 20,
                        y1: 20,
                        x2: 180,
                        y2: 160,
                        onSelectEnd: function (img, selection) {
                            $('input[name="x1"]').val(selection.x1);
                            $('input[name="y1"]').val(selection.y1);
                            $('input[name="x2"]').val(selection.x2);
                            $('input[name="y2"]').val(selection.y2);
                        }
                    });
                });
            }
        });

        return false;
    });
    
    $('.uploadPicBtn').click(function(event) {
        $(this).closest('form').find('.input_picture').trigger('click');
        event.preventDefault();
    });

    $('.input_picture').change(function() {
        $(this).parent('form').submit();
    });

    $('.upload_extra_pic').submit(function() {
        var element = $(this);
        var number = 1 + Math.floor(Math.random() * 1000000);
        element.closest('.img-div').addClass('loading');
        element.closest('.img-div').siblings('.image-loader').show();
        $(this).ajaxSubmit({
            success:function(data) {
                element.closest('.img-div').find('div.image-divs').css('background-image','url('+base_url+'upload/'+data+'?'+number+')');

                element.closest('.img-div').siblings('.image-loader').hide();
                element.closest('.img-div').removeClass('loading');
            }
        }); 
        return false;
    });
    
    $(".pay-btn").click(function() {
        var target = $(this).attr('href');
        $.ajax({
            type: 'POST',
            url: target,
            data: '',
            success: function(data) {
                $('#pay-modal').find('.modal-content').html(data);

                $('#pay-modal').modal({
                    keyboard: false,
                });

                $('#pay-modal').on('shown', function () {
                });

            }
        });
        return false;
    });
});