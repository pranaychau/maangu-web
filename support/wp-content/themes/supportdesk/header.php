<?php /** * The Header for our theme. * */ ?>
<!DOCTYPE html><html <?php language_attributes(); ?>><head><meta charset="<?php bloginfo('charset'); ?>" /><meta name="viewport" content="width=device-width, initial-scale=1" />
        <title><?php wp_title('-', true, 'right'); ?><?php bloginfo('name'); ?></title>
        <link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/images/favicon.ico" type="image/x-icon" />
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
        <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/7ee9684c.main.css"/>
        <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/style.css">
        <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery-1.8.2.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/common.js"></script>
        <script type="text/javascript">
            var fb_param = {};


            fb_param.value = '0.00';
            (function () {
                var fpw = document.createElement('script');
                fpw.async = true;
                fpw.src = (location.protocol == 'http:' ? 'http' : 'https') + '://connect.facebook.net/en_US/fp.js';
                var ref = document.getElementsByTagName('script')[0];
                ref.parentNode.insertBefore(fpw, ref);
            })();</script>    <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/offsite_event.php?id=6008250849506&amp;value=0" /></noscript>    <!--End facebook Tracking-->    <!--Begin google analytics-->    <script type="text/javascript">        var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-38566142-1']);
                _gaq.push(['_trackPageview']);
                (function () {
                    var ga = document.createElement('script');
                    ga.type = 'text/javascript';
                    ga.async = true;
                    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(ga, s);
                })();
    </script>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <nav class="navbar navbar-fixed-left navbar-minimal animate" role="navigation">
    <div class="navbar-toggler animate">       
    <span class="menu-icon"></span>    </div>  
 <ul class="navbar-menu animate">   
<li>  <a href="https://www.maangu.com/" class="animate">       
<span class="desc animate">Home</span> 
<span class="glyphicon glyphicon-home"></span>            </a>        </li>   
        <li>            <a href="https://www.maangu.com/company/about" class="animate">                <span class="desc animate">About us</span>                <span class="glyphicon glyphicon-user"></span>            </a>        </li>        <li>            <a href="https://www.maangu.com/pricing" class="animate">                <span class="desc animate">Pricing</span>                <span class="glyphicon glyphicon-usd"></span>            </a>        </li>        <li>            <a href="https://www.maangu.com/login" class="animate">                <span class="desc animate">Login</span>                <span class="glyphicon glyphicon-lock"></span>            </a>        </li>        <li>            <a href="https://www.maangu.com/register" class="animate">                <span class="desc animate">Register</span>                <span class="glyphicon glyphicon-plus"></span>            </a>        </li>        <li>            <a href="https://www.maangu.com/search" class="animate">                <span class="desc animate">Search Member</span>                <span class="glyphicon glyphicon-search"></span>            </a>        </li>        <li>            <a href="https://www.maangu.com/blog" class="animate">                <span class="desc animate">Our Blog</span>                <span class="glyphicon glyphicon-comment"></span>            </a>        </li>        <li>            <a href="https://www.maangu.com/company/press" class="animate">                <span class="desc animate">Press</span>                <span class="glyphicon glyphicon-bullhorn"></span>            </a>        </li>        <li>            <a href="https://www.maangu.com/company/csr" class="animate">                <span class="desc animate">Social Responsibilities</span>                <span class="glyphicon glyphicon-gift"></span>            </a>        </li>        <li>            <a href="https://www.maangu.com/company/contact" class="animate">                <span class="desc animate">Contact</span>                <span class="glyphicon glyphicon-envelope"></span>            </a>        </li>    </ul></nav><!-- /.nav --><div class="header-top">    <div class="container">        <div class="row">            <a href="https://www.maangu.com/support" class="logo"><span class="icon-maangu-logo" style="color:#f2f2f2;margin-left:-120px"></span></a>            <div class="header-login">                <div class="headerRegister">                    <a href="https://www.maangu.com/forgot-password" data-toggle="modal" data-target="#forgotPassword">Forgot Your Password?</a> |                     <a href="https://www.maangu.com/register" style="color:#f2f2f2;">Register</a>                    <!--<a href="register.php" data-toggle="modal" data-target="#newRegistration">Register</a> | -->                   <!-- <a href="#"><i class="fa fa-facebook"></i> Sign up with Facebook</a>-->                </div>                <!-- /.headerRegistrater -->                <form class="form-inline" id="login_form" role="form" method="post" action="https://www.maangu.com/pages/login">                    <div class="form-group">                        <!-- <label class="block" for="exampleInputEmail2">Email address</label> -->                        <!-- Please uncomment to make lable active -->                        <input type="email" name="email" class="form-control" placeholder="Email Address">                    </div>                    <div class="form-group">                        <!-- <label class="block" for="exampleInputPassword2">Password </label> -->                        <!-- Please uncomment to make lable active -->                        <input type="password" name="password" class="form-control" placeholder="Password">                    </div>                    <button type="submit" class="btn btn-default" style="margin-top:-10px;">Sign in</button>                </form>                <!-- /.form -->            </div>            <!-- /.header-login -->        </div>        <!-- /.row -->    </div>    <!-- /.container --></div>       
    <input type="hidden" value="https://www.maangu.com/" id="base_url" />          <!-- #primary-nav-mobile --><!-- #primary-nav --><!-- /#header -->