<?php 
$db=new DB();
$str="";
$msg=loadvariable('msg','');

$action=loadvariable('action','');
$a=loadvariable('a','list');

if($msg=='1')
{
		$str="Successfully Updated ";
		$cls="alert alert-success";
}
if($msg=='0')
{
		$str="Not Updated ....Problem Occured ";
		$cls="alert alert-danger";
}
if($a=='list')
{
	$SQL="SELECT * FROM `promo_codes`";
	$res=$db->get_results($SQL);
}

if($a=='edit')
{
	$id=loadvariable('id','');
	$SQL="SELECT * FROM `promo_codes` WHERE id='".$id."'";
	$res=$db->get_results($SQL);
	if($res[0]['Status']=='Y')
    {
		$chk1='Checked';
		$chk2='';
    }
	else
	{
		$chk2='Checked';
		$chk1='';
	}

}

$id=loadvariable('id','');
?>
   
	<script>
		$(document).ready(function(){
		    $('#myTable').dataTable();
		    //myTable is you table id.
		});

	</script>

<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
	  <section class="content">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           Promo Code 
            <small>Information</small>
          </h1>
          <ol class="breadcrumb">

	  <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="index.php?p=users">Promo code</a></li>
			<li class="active">Promo table</li>  	
          </ol>
        </section>

       
      <div class="content">
         <!-- Main content -->
          <div class="row">
            <div class="col-xs-12">
             <div class="box">
			  <a class="btn btn-info pull-right" href="index.php?p=promo_code&a=add">Add New Promo Codes</a>
	</div> 
		<?php 
			if($str!="")
			{?>
		    	<div class="<?php echo  $cls;?>" role="alert">
          <a href="#" class="alert-link"><?php  echo $str;?></a>
        </div>
			<?php }
			?>
                <div class="box-header">
                  <h3 class="box-title"></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
				<?php if($a=='list')
				{?>
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No.</th>
                       
                        <th>Promo code</th>
                        <th>Created on</th>
                        <th>Description</th>
                        <th>Waived for</th>
						<th>Validity</th>
						<th>Status</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
					<?php 
					$date_today=date('Y-m-d');
				
					for($i=0;$i<count($res);$i++)
					{ 
				
					
					?>
                      <tr <?php if( strtotime($date_today) > strtotime($res[$i]['Expire']))
					{
					echo 'style="background-color:red"';
					}?> >
                     		<td>	 <?php  echo $i+1; ?></td>
							<td> <?php echo $res[$i]['promo_code']; ?></td>
			             	<td> <?php echo $res[$i]['created_date']; ?></td>
			             	<td> <?php echo $res[$i]['promo_description']; ?></td>
							<td> <?php echo $res[$i]['waived_by']; ?></td>
							<td> <?php echo $res[$i]['Expire']; ?></td>
							<td> 
							 <?php if($res[$i]['Status']=='Y')
								{?>
									<a class="btn btn-success" style="padding:2px;"
									href="../model/promo_code.php?a=update&start=<?php echo $start;?>&status=N&id=<?php echo $res[$i]['id']; ?>" title="Click to IN-Active Mode"><i class="fa fa-check"></i></a>
								<?php }?>
								<?php
								if($res[$i]['Status']=='N')	
								{?>
									<a class="btn btn-danger" style="padding:2px;"
									href="../model/promo_code.php?a=update&start=<?php echo $start;?>&status=Y&id=<?php echo $res[$i]['id'];?>" title="Click to Active Mode"><i class="fa fa-hand-pointer-o"></i></a>
								<?php }?></td>
					   		<td>
							<a class="btn btn-success" href="index.php?p=promo_code&a=edit&id=<?php echo $res[$i]['id']; ?>"><i class="fa fa-edit"></i>&nbsp; Edit</a>
							<a class="btn btn-danger" href="../model/promo_code.php?a=delete&id=<?php echo $res[$i]['id']; ?>"><i class="fa fa-remove"></i>&nbsp; Delete</a>
							</td>
                      </tr>
					  <?php }?>
                     </tbody>

            </table>
				  <?php }?><!-----LIST CLOSE-------------->
				   
				   <?php 			  
				  if($a=='add')
				  { ?>
				  <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Enter Details For Promo Code </h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="post" action="../model/promo_code.php">
				<input type="hidden" name="a" value="add">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Promo Code</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control"  name="promo_name"id="inputEmail3" placeholder="Promo code" required>
                      </div>
                    </div>
					<div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Promo Code Description</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" name="promo_description" id="promo_description" placeholder="Promo Code Description."required>
                      </div>
                    </div>
          <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">No. of days waived off</label>
                      <div class="col-sm-6">
                        <input type="number" class="form-control" name="waived_by" id="waived_by" placeholder="No. of days waived off."required>
                      </div>
                    </div>
					 <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Validity</label>
                      <div class="col-sm-6">
                        <input type="date" class="form-control" name="promo_validity"  id="" placeholder="" required>
                      </div>
                    </div>
					 
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-default">Cancel</button>
                    <button type="submit" class="btn btn-info" id="btnSubmit">Add Promo Code</button>
                  </div><!-- /.box-footer -->
                </form>
				
				<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
					<!--<script type="text/javascript">
								$(function () {
								
							$("#btnSubmit").click(function () {
								var password = $("#txtPassword").val();
								var confirmPassword = $("#txtConfirmPassword").val();
								if (password != confirmPassword) {
									alert("Passwords do not match.");
									return false;
								}
								return true;
							});
						});
					</script>-->
								
              </div>
				  <?php }
				  ?>
				  
				 <?php if($a=='edit')
				  { 
				// print_r($res);
				  ?>
				  <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Update Details For Promo Codes</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="post" action="../model/promo_code.php">
				<input type="hidden" name="a" value="edit">
				<input type="hidden" name="id" value="<?php echo $res[0]['id']; ?>">
                  <div class="box-body">
                    
					       <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Promo Code</label>
                      <div class="col-sm-6">
                        <input type="text"   value="<?php echo $res[0]['promo_code'] ?>"class="form-control" name="promo_name" id="inputEmail3" placeholder="Name ">
                      </div>
                    </div>

                  <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Promo Code Description</label>
                      <div class="col-sm-6">
                        <input type="text"  value="<?php echo $res[0]['promo_description'] ?>"class="form-control" name="promo_description" id="inputEmail3" placeholder="Email ">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">No. of days waived off</label>
                      <div class="col-sm-6">
                        <input type="text"  value="<?php echo $res[0]['waived_by'] ?>"class="form-control" name="waived_by" id="inputEmail3" placeholder=" No. of days waived off">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">validity</label>
                      <div class="col-sm-6">
                        <input type="date"  value="<?php echo $res[0]['validity'] ?>"class="form-control" name="promo_validity" id="inputEmail3" placeholder="validity ">
                      </div>
                    </div>
                    <!-- <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">STATUS</label>
                      <div class="col-sm-6">
					   <label for="inputEmail3" >
					  <input type="radio" name="status" value="Y" <?php //echo $chk1;?>>  Y</label>
					   <label for="inputEmail3" >
					  <input type="radio" name="status" value="N" <?php //echo $chk2; ?>>  N</label>
                      </div>
                    </div>-->
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-default">Cancel</button>
                    <button type="submit" class="btn btn-info">Update Promo Code</button>
                  </div><!-- /.box-footer -->
                </form>
              </div>
				  <?php }
				  ?>


          
                </div><!-- /.box-body -->
				
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
		  </div>
		  
        </section><!-- /.content -->
		
      </div><!-- /.content-wrapper --><!-- /.content -->
	



