<!--This is the public view of the profile-->
<?php defined('SYSPATH') or die('No direct script access.');
class Controller_Profile extends Controller_Template {
    public $template = 'templates/members';

    public function before() {
        parent::before();
        if(!Auth::instance()->logged_in()) { //if not login redirect to login page
            $this->request->redirect('pages/login');
        }
        
        $user = Auth::instance()->get_user();
        $actions = array('unique_username', 'profile_pic', 'add_pic', 'webcam_pic');

        if($user->firsttime_done != 'done' && !in_array($this->request->action(), $actions)) {
            Auth::instance()->get_user()->check_registration_steps();
        }
        
        require_once Kohana::find_file('classes', 'libs/stripe-php/lib/Stripe');
        //if request is ajax don't load the template
        if(!$this->request->is_ajax() && $this->request->is_initial()) {
            $header_data['featured_dates'] = DB::select(array(DB::expr('Distinct featured_date'), 'featured_date'))
            ->from('features')
            ->execute()->as_array();
            
            $this->template->title = Auth::instance()->get_user()->member->first_name .' '.Auth::instance()->get_user()->member->last_name;
            $this->template->header = View::factory('templates/members-header', $header_data);
            $this->template->footer = View::factory('templates/footer');
        }

        $this->post = new Model_Post;
        $this->activity = new Model_Activity;
    }
    
    public function action_profile() {
        $member = ORM::factory('member', Auth::instance()->get_user()->member->id); //get current member object
        
        if ($this->request->post()) { // if post request
            $post_data = $this->request->post();
            $error = false;
            
            if($post_data['location_chk'] != 'done') { //if correct location is used from google places
                Session::instance()->set('error', 'Please choose a proper location.');
            } else {
                if ($this->request->post('ipintoo_username')) { // if post request
                    $user = Auth::instance()->get_user();
                    $user->ipintoo_username = $this->request->post('ipintoo_username');
                    $user->save();
                }
                //save member details
                $member->values($post_data);
                $changed = $member->changed();
                $original_values = $member->original_values();
                $member->save();
                
                $i = 0;
                $show_column = Kohana::$config->load('profile')->get('show_column');
                foreach($changed as $change) {
                    if(array_key_exists($change, $show_column)) {
                        $change_config = Kohana::$config->load('profile')->get($change);
                        if(!empty($original_values[$change])) {
                            if(!empty($change_config)) {
                                    $post_content = "has updated ".$show_column[$change].
                                " from '".$change_config[$original_values[$change]].
                                "' to '".$change_config[$member->$change]."'";
                                
                            } else {
                                if(($show_column[$change] == 'salary_nation') OR ($show_column[$change] == 'salary') ){
                                    if($i == '0'){
                                    $post_content = "has updated ".$show_column['salary']." from ";
                                    if($original_values['salary_nation'] == '2'){
                                        $post_content .= "'Rs.  ".$original_values['salary']."'";
                                    }else{
                                        $post_content .= "'$ ".$original_values['salary']."'";
                                    }
                                    $post_content .= " to ";
                                    if($member->salary_nation == '2'){
                                        $post_content .= "'Rs.  ".$member->salary."'";
                                    }else{
                                        $post_content .= "'$ ".$member->salary."'";
                                    }
                                    }else{
                                        $post_content = '';
                                    }
                                    $i++;
                                }else{
                                    $post_content = "has updated ".$show_column[$change]." from '".$original_values[$change]."' to '".$member->$change."'";
                                }
                                
                            }
                        
                        } else {
                            if(!empty($change_config)) {
                                $post_content = "has added ".$show_column[$change].
                                " as '".$change_config[$member->$change]."'";
                            } else {
                                if(($show_column[$change] == 'salary') OR ($show_column[$change] == 'salary_nation')){
                                    if($i == '0'){
                                        $post_content = "has added ".$show_column['salary']." as ";
                                        if($member->salary_nation == '2'){
                                            $post_content .= 'Rs. '.$member->salary;
                                        }else{
                                            $post_content .= '$ '.$member->salary;
                                        }
                                        $i++;
                                    }else{
                                        $post_content = '';
                                    }
                                }else{
                                    $post_content = "has added ".$show_column[$change]." as '".$member->$change."'";
                                }
                            }
                        }
                        if(!empty($post_content)){
                                $this->post->new_post('profile details', $post_content);
                        }
                    }
                }

                Session::instance()->set('success', 'Your profile details have been updated. Now, please take a moment to check your partner details.');
            }
        }
        
        $data['member'] = $member;
        $data['edit'] = "profile"; //identifier for profile edit page

        $this->template->content = View::factory('members/edit_profile', $data);
    }

    public function action_add_ipintoo_profile() {
        if ($this->request->post('ipintoo_username')) { // if post request
            $user = Auth::instance()->get_user();
            $user->ipintoo_username = $this->request->post('ipintoo_username');
            $user->save();
            
            Session::instance()->set('success', 'Callitme account linked successfully');
        }

        if($this->request->post('settings')) {
            $this->request->redirect(url::base().'settings/account');
        } else {
            $this->request->redirect(url::base());
        }
    }

    public function action_partner() {
        $member = ORM::factory('member', Auth::instance()->get_user()->member->id);
        
        if ($this->request->post()) { // if post request
            $post_data = $this->request->post();
            if ($member->partner_id) { //if partner is already set, update the data
                $partner = ORM::factory('partner', $member->partner_id);
            } else { //create new partner record,if not exists
                $partner = ORM::factory('partner');
            }
            $partner->values($post_data);
            $changed = $partner->changed();

            $original_values = $partner->original_values();
            $partner->save();

            if (!$member->partner_id) {
                $member->partner_id = $partner->id; //if new record insert partner_id in members table
                $member->save();
            }
            
            $show_column = Kohana::$config->load('profile')->get('show_column');
            foreach($changed as $change) {
                if(array_key_exists($change, $show_column) && !empty($partner->$change)) {
                    $change_config = Kohana::$config->load('profile')->get($change);
                    if(!empty($original_values[$change])) {
                        if(!empty($change_config)) {
                            $post_content = "has updated Desired Partner's ".$show_column[$change].
                            " from '".$change_config[$original_values[$change]].
                            "' to '".$change_config[$partner->$change]."'";
                        } else {
                            $post_content = "has updated Desired Partner's ".$show_column[$change]." from '".$original_values[$change]."' to '".$partner->$change."'";
                        }
                    
                    } else {
                        if(!empty($change_config)) {
                            $post_content = "has added Desired Partner's ".$show_column[$change].
                            " as '".$change_config[$partner->$change]."'";
                        } else {
                            $post_content = "has added Desired Partner's ".$show_column[$change]." as '".$partner->$change."'";
                        }
                    }
                    
                    $this->post->new_post('partner details', $post_content);
                }
            }

            Session::instance()->set('success', 'You have updated your desired partner details. Now, please see if you have uploaded your beautiful photos. You can add one profile photo and three additional photos. Make sure your face is clearly visible. Pictures containing anything other than yourself are not allowed and will be deleted by the system.');
        }
        
        $data['member'] = $member;
        $data['edit'] = "partner";

        $this->template->content = View::factory('members/edit_profile', $data);
    }
    
    public function action_username() {
        $member = Auth::instance()->get_user()->member;
        
        if ($this->request->post('username')) {
            
            //check if username is already taken
            $post_data = $this->request->post();
            if(!empty($post_data['username']) && $post_data['username'] != $member->user->username) {
                if($member->user->check_username($post_data['username'])) {
                    Session::instance()->set('error', 'This Username is already taken. Please select something else.');
                } else {
                    //save data
                    $user = Auth::instance()->get_user();
                    $user->values(array('username' => $post_data['username']));
                    $user->save();

                    $this->post->new_post('account details', '', "has updated username.");
                    $base_url_site = str_replace('://','',strstr(url::base(), ':'));
                    Session::instance()->set('success', 'Your Username has been updated. Now your profile can be viewed and searched by your Username. Your profile link is '.$base_url_site.$post_data['username']);
                }
            }
            
            $this->request->redirect(url::base().'settings/account');
        }
        
        if(!$this->request->is_ajax() && $this->request->is_initial()) {
            $this->template->content = View::factory('profile/account_settings');
        } else {
            $this->auto_render = false;
            $data['member'] = $member;
            $data['suggestions'] = $member->user->username_suggestions();
            
            echo View::factory('profile/change_username_form', $data);
        }
    }
    
    public function action_account() {
        $member = Auth::instance()->get_user()->member;
        
        if ($this->request->post('email')) {
            
            try {
                $post_data = $this->request->post();
                if($post_data['email'] != $member->user->email) {
                    $user = ORM::factory('user')
                        ->where('email', '=', $post_data['email'])
                        ->where('id', '!=', $member->user->id)
                        ->find();
                        
                    if($user->id) {
                        Session::instance()->set('error', 'This email is already registered.');
                    } else {
                        //save data
                        $user = Auth::instance()->get_user();
                        $user->values(array('new_email' => $post_data['email']));
                        $user->save();
                    
                        // Token data
                        $data = array(
                            'user_id'    => $user->pk(),
                            'expires'    => time() + 1209600,
                            'created'    => time(),
                            'type'       => 'change_email'
                        );

                        // Create a new activation token
                        $token = ORM::factory('user_token')
                            ->values($data)
                            ->create();

                        $email = base64_encode($user->new_email);

                        $link = url::base()."pages/change_email/".$email."/".$token->token;
                        
                        //send activation email
                        
                        $send_email = Email::factory('Maangu - Change Email Address Confirmation')
                            ->message(View::factory('change_email_mail', array('link' => $link))->render(), 'text/html')
                            ->to($user->email)
                            ->from('noreply@maangu.com', 'Maangu')
                            ->send();
                        
                        $send_email = Email::factory('Change Email Address request')
                            ->message(View::factory('alert_email_mail', array('new_email' => $user->new_email))->render(), 'text/html')
                            ->to($user->email)
                            ->from('noreply@maangu.com', 'Maangu')
                            ->send();
                    }
                }
                
                $this->request->redirect(url::base()."settings/account");
            } catch (ORM_Validation_Exception $e) { 
                Session::instance()->set('error', $e->errors('models'));
            }
            
        }
        
        $data['member'] = $member;

        $this->template->content = View::factory('profile/account_settings', $data);
    }

    public function action_deactivate() {
        if($this->request->post('confirm') && $this->request->post('reason')) {
            $member = Auth::instance()->get_user()->member;
            $member->is_deleted = 1;
            $member->delete_expires = date("Y-m-d", strtotime("+30 days"));
            $member->save();


            $send_email = Email::factory('Deactivate Profile Mail')
                ->message(View::factory('mails/deactivate_profile_mail_admin', array('user' => $member->user,
                    'reason' => $this->request->post('reason')))->render(), 'text/html')
                ->to('nikhilmonga2002@gmail.com')
                ->from('noreply@maangu.com', 'Maangu')
                ->send();

            $send_email = Email::factory('Deactivate Profile Mail')
                ->message(View::factory('mails/deactivate_profile_mail', array('user' => $member->user))->render(), 'text/html')
                ->to($member->user->email)
                ->from('noreply@maangu.com', 'Maangu')
                ->send();

            Session::instance()->set('success', 'Your account has been deactivated. <br /> Remember, you can activate your \
            account anytime by visiting Maangu and logging into your account.');

            $this->request->redirect(url::base().'pages/logout');
        }

        $this->request->redirect(url::base()."settings/account");
    }

    public function action_subscription() {
        $member = Auth::instance()->get_user()->member;
        $data['member'] = $member;

        $this->template->content = View::factory('profile/subscription', $data);
    }

    public function action_password() {
        $member = Auth::instance()->get_user()->member;
        
        if ($this->request->post('old_password')) {
            
            if (Auth::instance()->check_password( $this->request->post('old_password') )) {
                //if old password match, save new password
                $user = $member->user;
                $user->values($this->request->post());
                $user->save();
                Session::instance()->set('success', 'Your Password has been updated');
                
            } else {
                Session::instance()->set('error', 'Incorrect Password.');
            }
            
            $this->request->redirect(url::base().'settings/account');
        }
        
        if(!$this->request->is_ajax() && $this->request->is_initial()) {
            $this->template->content = View::factory('profile/account_settings');
        } else {
            $this->auto_render = false;
            echo View::factory('profile/change_password_form');
        }
    }
    
    public function action_unique_username() {
        // check if the username is already taken or not
        $this->auto_render = false;
        if($this->request->post()) {
            $user = ORM::factory('user')
                        ->where('username', '=', $this->request->post('username'))
                        ->where('id', '!=', Auth::instance()->get_user()->id)
                        ->find();
            
            if($user->id) {
                echo '1'; 
            } else {
                echo '0';
            }
        }
    }
    
    public function action_set_notification() {
        //set email notification settings
        $this->auto_render = false;
        if($this->request->post('notification')) {
            $user = ORM::factory('user', Auth::instance()->get_user()->id);
            
            if($this->request->post('notification') == 'true') {
                $user->email_notification = 1; 
            } else {
                $user->email_notification = 0;
            }
            
            $user->save();
            
            echo "done";
        }
    }

    public function action_webcam_pic() {
        $this->auto_render = false;
        
        $user = Auth::instance()->get_user();
        if(!$user->member->photo->id) {
            $photo = ORM::factory('photo');
            $photo->path = str_replace('.', '', explode('@', $user->email)[0]).'/';
            $photo->save();
            
            $user->member->photo_id = $photo->id;
            $user->member->save();
        } else {
            $photo = $user->member->photo;
        }

        $upload_folder = DOCROOT."upload/".$photo->path;
        
        //The JPEG snapshot is sent as raw input:
        $input = file_get_contents('php://input');
        $filename = "pp-".strtotime('now')."_o.jpg"; //original profile pic

        if(md5($input) == '7d4df9cc423720b7f1f3d672b89362be'){
            // Blank image. We don't need this one.
            echo json_encode(array('error' => '1', 'message' => 'Blank Image'));
            return;
        }

        $result = file_put_contents($upload_folder.$filename, $input);
        if (!$result) {
            echo json_encode(array('error' => '1', 'message' => 'Failed to save the image. Please try again'));
            return;
        }

        $photo = $this->upload_pp($upload_folder.$filename, '', true);
        echo json_encode(array('status' => '1', 'message' => 'Success', 'filename' => $filename));
    }

    public function action_profile_pic() {
        $this->auto_render = false;

        if($this->request->post()) {
            $user = Auth::instance()->get_user();
            
            if(!$user->member->photo->id) {
                $photo = ORM::factory('photo');
                $photo->path = str_replace('.', '', explode('@', $user->email)[0]).'/';
                $photo->save();
                
                $user->member->photo_id = $photo->id;
                $user->member->save();
            } else {
                $photo = $user->member->photo;
            }
            
            $upload_folder = DOCROOT."upload/".$photo->path;
            
            if($this->request->post('crop')) {
                $post_data = $this->request->post();
                $picture = $upload_folder.basename($this->request->post('imag_name')); //upload picture
                $photo = $this->upload_pp($picture, $post_data);

                $this->request->redirect($this->request->referrer());
            } else {
                if (!file_exists($upload_folder)) {
                    mkdir($upload_folder, 0777, true);
                }
                
                $picture = Upload::save($_FILES['picture'], null , $upload_folder); //upload picture
                $str = Text::random();
                $original = "pp-".strtotime('now')."_o.jpg"; //original profile pic
                //resize to different sizes
                $image = Image::factory($picture);
                $image->resize(600, 500);
                $image->save($upload_folder.$original);

                $data['width']  = $image->width + 60;
                $data['height']  = $image->height + 190;
                $data['image'] = $photo->path.$original;
                
                try {
                    unlink($picture);
                } catch (Exception $e) {}
                
                echo json_encode($data);
            }

        } else {
            $this->request->redirect($this->request->referrer());
        }

    }

    private function upload_pp($picture, $post_data, $cam = false) {
        $member = Auth::instance()->get_user()->member;
        
        $photo = $member->photo;
        $upload_folder = DOCROOT."upload/".$photo->path;

        try {
            //delete previous profile picture if exists
            if (file_exists($upload_folder.$photo->profile_pic))
                unlink($upload_folder.$photo->profile_pic);
            if (file_exists($upload_folder.$photo->profile_pic_m))
                unlink($upload_folder.$photo->profile_pic_m);
            if (file_exists($upload_folder.$photo->profile_pic_s))
                unlink($upload_folder.$photo->profile_pic_s);
            if (file_exists($upload_folder.$photo->profile_pic_o))
                unlink($upload_folder.$photo->profile_pic_o);
                
        } catch(Exception $e) { }

        $image = Image::factory($picture);

        $strtotime = strtotime('now');
        $name = "pp-".$strtotime.".jpg"; //main profile pic
        $name_s = "pp-".$strtotime."_s.jpg"; //small pic
        $name_m = "pp-".$strtotime."_m.jpg"; //mini pic

        if(!empty($post_data)) {
            $post_data['x1'] = !empty($post_data['x1']) ? $post_data['x1'] : 0;
            $post_data['y1'] = !empty($post_data['y1']) ? $post_data['y1'] : 0;
            $post_data['x2'] = !empty($post_data['x2']) ? $post_data['x2'] : $image->width;
            $post_data['y2'] = !empty($post_data['y2']) ? $post_data['y2'] : $image->height;
            
            $new_w = $post_data['x2'] - $post_data['x1'];
            $new_h = $post_data['y2'] - $post_data['y1'];

            $image->crop($new_w, $new_h, $post_data['x1'], $post_data['y1']);
            $image->save($upload_folder.$name);
        }

        $image->resize(270, null);
        $image->save($upload_folder.$name);
        
        $image->resize(null, 63);
        $image->save($upload_folder.$name_s);

        $image->resize(null, 50);
        $image->save($upload_folder.$name_m);

        //update image names in database
        $photo->profile_pic = basename($name);
        $photo->profile_pic_m = basename($name_m);
        $photo->profile_pic_s = basename($name_s);
        $photo->profile_pic_o = basename($picture);
        $photo->save();

        /* DB::delete(ORM::factory('ask_photo')->table_name())
            ->where('asked_to', '=', $member->id)
            ->execute(); */

        $this->post->delete_post('profile_pic', $member);
        $this->activity->delete_activity('profile_pic', $member);

        if($member->user->firsttime_done !== '0'){
            $post = $this->post->new_post('profile_pic', url::base().'upload/'.$photo->path.$photo->profile_pic,  "has updated Profile picture.");
            $this->activity->new_activity('profile_pic', 'updated Profile picture', $post->id, '');
        } else {
            if(!$cam) {
                $this->request->redirect(url::base()."firsttime/step1");
            }
        }

        return $photo;
    }

    public function action_add_pic() {
        $this->auto_render = false;
        if( $this->request->post() ) {
            if($this->request->post('user_image_id') > 0){
                $member = ORM::factory('member', $this->request->post('user_image_id'));
            }else{
                $member = Auth::instance()->get_user()->member;
            }
            
            if(!$member->photo->id) {
                $photo = ORM::factory('photo');
                $photo->path = str_replace('.', '', explode('@', $member->user->email)[0]).'/';
                $photo->save();
                
                $member->photo_id = $photo->id;
                $member->save();
            } else {
                $photo = $member->photo;
            }

            $upload_folder = DOCROOT."upload/".$photo->path;
            if($photo->path && !file_exists($upload_folder."original/")) {
                mkdir($upload_folder."original/", 0777, true);
            }
            
            $strtotime = strtotime('now');
            $name = 'picture'.$this->request->post('name')."-".$strtotime.".jpg";
            $picture = Upload::save($_FILES['picture'], $name , $upload_folder."original/");
            
            $exif = exif_read_data($picture);
            if(isset($exif['Orientation'])) {
                $image = Image::factory($picture);
                $ort = $exif['Orientation'];
                switch($ort)
                {
                    case 1: // nothing
                    break;

                    case 2: // horizontal flip
                        $image->flip(Image::HORIZONTAL);
                    break;
                                           
                    case 3: // 180 rotate left
                        $image->rotate(-180);
                    break;
                               
                    case 4: // vertical flip
                        $image->flip(Image::VERTICAL);
                    break;
                           
                    case 5: // vertical flip + 90 rotate right
                        $image->flip(Image::VERTICAL);
                        $image->rotate(90);
                    break;
                           
                    case 6: // 90 rotate right
                        $image->rotate(90);
                    break;
                           
                    case 7: // horizontal flip + 90 rotate right
                        $image->flip(Image::HORIZONTAL);
                        $image->rotate(90);
                    break;
                           
                    case 8:    // 90 rotate left
                        $image->rotate(-90);
                    break;
                }
                $image->save();
            }
            
            try {
                //delete previous image id exists
                if($this->request->post('name') == 1 ) {
                    if (file_exists($upload_folder."original/".$photo->picture1))
                        unlink(DOCROOT."original/".$photo->picture1);
                    if (file_exists($upload_folder.$photo->picture1))
                        unlink($upload_folder.$photo->picture1);
                } else if ($this->request->post('name') == 2) {
                    if (file_exists($upload_folder."original/".$photo->picture2))
                        unlink($upload_folder."original/".$photo->picture2);
                    if (file_exists($upload_folder.$photo->picture2))
                        unlink($upload_folder.$photo->picture2);
                } else {
                    if (file_exists($upload_folder."original/".$photo->picture3))
                        unlink($upload_folder."original/".$photo->picture3);
                    if (file_exists($upload_folder.$photo->picture3))
                        unlink($upload_folder.$photo->picture3);
                }
            } catch(Exception $e) { }
            
            if($this->request->post('name') == 1 ) {
                $photo->picture1 = basename($picture);
            } else if ($this->request->post('name') == 2) {
                $photo->picture2 = basename($picture);
            } else {
                $photo->picture3 = basename($picture);
            }
            
            $photo->save();

            $this->post->delete_post('new_pic'.$this->request->post('name'), $member);
            $this->activity->delete_activity('new_pic'.$this->request->post('name'), $member);

            if($this->request->post('firsttime') != '7' ){
                $post = $this->post->new_post('new_pic'.$this->request->post('name'), "has added new picture.".url::base().'upload/original/'.basename($picture));
                $this->activity->new_activity('new_pic'.$this->request->post('name'), 'has added new picture', $post->id, '');
            }

            $image = Image::factory($picture);
            $image->resize(700, 600);
            $image->save();
            
            $image = Image::factory($picture);
            $image->resize(185, 185, Image::PRECISE);
            $image->save($upload_folder.$name);
            
            echo $photo->path.$name;
        }
        
    }

    public function action_delete_pic() {
       
        if( $this->request->post() ) {
            
            if($this->request->post('user_image_id') > 0){
                $member = ORM::factory('member',$this->request->post('user_image_id'));
            }else{
                $member = ORM::factory('member', Auth::instance()->get_user()->member->id);
            }
            
            if ($member->photo_id) {
                $photo = ORM::factory('photo', $member->photo_id);
                
                try {
                    //delete previous image id exists
                    if($this->request->post('name') == 1 ) {
                        if (file_exists(DOCROOT."upload/original/".$photo->picture1))
                            unlink(DOCROOT."upload/original/".$photo->picture1);
                        if (file_exists(DOCROOT."upload/".$photo->picture1))
                            unlink(DOCROOT."upload/".$photo->picture1);
                        $photo->picture1 = '';
                    } else if ($this->request->post('name') == 2) {
                        if (file_exists(DOCROOT."upload/original/".$photo->picture2))
                            unlink(DOCROOT."upload/original/".$photo->picture2);
                        if (file_exists(DOCROOT."upload/".$photo->picture2))
                            unlink(DOCROOT."upload/".$photo->picture2);
                        $photo->picture2 = '';
                    } else {
                        if (file_exists(DOCROOT."upload/original/".$photo->picture3))
                            unlink(DOCROOT."upload/original/".$photo->picture3);
                        if (file_exists(DOCROOT."upload/".$photo->picture3))
                            unlink(DOCROOT."upload/".$photo->picture3);
                        $photo->picture3 = '';
                    }
                    
                    $photo->save();
                    
                    $this->post->delete_post('new_pic'.$this->request->post('name'), $member);
                    $this->activity->delete_activity('new_pic'.$this->request->post('name'), $member);
                } catch(Exception $e) { }
                
            }

        }
        if($this->request->post('firsttime') == 1 ){
            $this->request->redirect(url::base()."members/firsttime");
        }
        $this->request->redirect(url::base()."/".$member->user->username ."/photos");
    }
    
    
    public function action_monthly_subscription() {
        $user = Auth::instance()->get_user();
    
        if($this->request->post()) 
            {
                $country=$this->request->post('countryname');
                  if($country=='India' || $country=='india'|| $country=='INDIA')
                  {
                      $currency='INR';
                      
                      $amount=Kohana::$config->load('stripe')->get('monthly_charge_rs');
                  }
                else {
                    $currency='usd';
                    
                   $amount= Kohana::$config->load('stripe')->get('monthly_charge');
                   
                }
            try { // Use Stripe's bindings... 
                Stripe::setApiKey(Kohana::$config->load('stripe')->get('secret_key'));
                
                $charge = Stripe_Charge::create(array(
                    "amount" => $amount, // amount in cents, again
                    "currency" => $currency,
                    "card" => $this->request->post('stripeToken'),
                    "description" =>  $user->email)
                );
        
                if($user->payment_expires > date("Y-m-d H:i:s")) {
                    $user->payment_expires = date("Y-m-d H:i:s",
                        mktime(23, 59, 59, date("m", strtotime($user->payment_expires))+1,
                            date("d", strtotime($user->payment_expires)),
                            date("Y", strtotime($user->payment_expires))
                        )
                    );
                } else {
                    $user->payment_expires = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m")+1  , date("d")-1, date("Y")));
                }
                
                $user->last_payment = date("Y-m-d H:i:s");
                $user->save();
                
                $payment = ORM::factory('payment');
                $payment->user_id = $user->id;
                $payment->type = 'monthly';
                $payment->price = '10.00';
                $payment->payment_date = date("Y-m-d H:i:s");
                $payment->save();
                
                Session::instance()->set('success', 'Your subscription has been updated successfully');
                
                $this->request->redirect(url::base().'settings/subscription');
                
            } catch(Stripe_CardError $e) {
                // Since it's a decline, Stripe_CardError will be caught
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            } catch (Stripe_InvalidRequestError $e) {
                // Invalid parameters were supplied to Stripe's API 
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            } catch (Stripe_AuthenticationError $e) { 
                // Authentication with Stripe's API failed // (maybe you changed API keys recently) 
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            } catch (Stripe_ApiConnectionError $e) {
                // Network communication with Stripe failed 
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            } catch (Stripe_Error $e) {
                // Display a very generic error to the user, and maybe send // yourself an email 
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe 
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            }
            
            if(!empty($user->last_payment)) {
                $this->request->redirect(url::base() . 'settings/subscription');
            } else {
                $this->request->redirect(url::base() . 'register/firsttime_subscription');
            }
            
        } else {
            $this->auto_render = false;
            $data['page'] = 'monthly';
            $data['action'] = url::base().'profile/monthly_subscription';
            echo View::factory('pay_form', $data);
        }
        
    }
    
    public function action_feature()
            {
        
        if($this->request->post('feature-date')) {
            
            try { // Use Stripe's bindings... 
                Stripe::setApiKey(Kohana::$config->load('stripe')->get('secret_key'));
                
                $charge = Stripe_Charge::create(array(
                    "amount" => Kohana::$config->load('stripe')->get('feature_charge'), // amount in cents, again
                    "currency" => "usd",
                    "card" => $this->request->post('stripeToken'))
                );
                
                $member = Auth::instance()->get_user()->member;

                $feature = ORM::factory('feature');
                $feature->member_id = $member->id;
                $feature->time = date('Y-m-d H:i:s');
                $feature->featured_date = $this->request->post('feature-date');
                $feature->save();
                
                Session::instance()->set('success', 'Payment successfull.');
                
            } catch(Stripe_CardError $e) {
                // Since it's a decline, Stripe_CardError will be caught
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            } catch (Stripe_InvalidRequestError $e) {
                // Invalid parameters were supplied to Stripe's API 
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            } catch (Stripe_AuthenticationError $e) { 
                // Authentication with Stripe's API failed // (maybe you changed API keys recently) 
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            } catch (Stripe_ApiConnectionError $e) {
                // Network communication with Stripe failed 
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            } catch (Stripe_Error $e) {
                // Display a very generic error to the user, and maybe send // yourself an email 
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe 
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            }
            
            $this->request->redirect(url::base().'settings/subscription');
            
        } else {
            $this->auto_render = false;
            $data['page'] = 'feature';
            $data['action'] = url::base().'profile/feature';
            echo View::factory('pay_form', $data);
        }
    }
    
    
    
    /*****************email notification *********************************/
    
    
    public function action_emailnotifaction() 
        {    
             $username = $this->request->param('username');
             $user = ORM::factory('user', array('username' => $username));
             $member = Auth::instance()->get_user()->member;
             $data['userdetils']=ORM::factory('user_detail')
                                 ->where('member_id', '=', $member->id)                                            
                                 ->find_all()                    
                                 ->as_array();
                        
       
        
         $user = Auth::instance()->get_user();    
            if($this->request->post()) 
            {

                   /* $req_alert = $this->request->post('req_alert') ;
                    $msg_alert = $this->request->post('msg_alert') ;
                    $rec_alert = $this->request->post('rec_alert') ;
                    $friend_alert = $this->request->post('friend_alert');*/

                     $req_alert= $this->request->post('req_alert') ? 1 : 0;
                     $msg_alert = $this->request->post('msg_alert') ? 1 : 0;
                     $rec_alert = $this->request->post('rec_alert') ? 1 : 0;
                     $friend_alert = $this->request->post('friend_alert') ? 1 : 0;
                     $query =  DB::update('user_details')
                             ->set(array('req_alert' =>$req_alert,'msg_alert'=>$msg_alert,'friend_alert'=>$friend_alert,'rec_alert'=>$friend_alert))
                             ->where('member_id', '=',$member->id)
                             ->execute();                                                          
                    Session::instance()->set('success', 'Your settings have been updated');
                    $this->request->redirect(url::base()."profile/emailnotifaction");
            }    
          $this->template->content = View::factory('profile/emailnotifaction',$data);
       }

}