$(document).click(function (event) {
    if (!$(event.target).hasClass('user-search-results')) {
        $(".user-search-results").hide();
    }
});

$(document).ready(function () {

    var prev_value = '';
    var prev_result = '';

    notification();
    setInterval(function () {
        notification();
    }, 10000);

    var base_url = $('#base_url').val();
    $('#search-query').keyup(function () {
        var query = $(this).val();
        query = $.trim(query);

        if (query != '') {
            $.ajax({
                type: 'get',
                url: base_url + "members/search",
                data: 'query=' + query,
                success: function (data)
                {
                    $('.user-search-results').html(data);
                    $('.user-search-results').show();
                }
            });
        }

    });
    $('#search-query').keyup(function () {
        var query = $(this).val();
        if (query == '') {
            $('.user-search-results').hide();
        }


    });

    $('#msg-noti').closest('li').on('show.bs.dropdown', function () {
        var element = $('#msg-noti');
        element.closest('li').find('ul').html('<li style="text-align:center;padding:10px;"><img src="' + base_url + 'assets/images/loader.gif" /></li>');

        var target = element.attr('href');
        $.ajax({
            type: 'POST',
            url: target,
            data: '',
            success: function (data) {
                element.closest('li').find('ul').html(data);
            },
            error: function () {
                element.closest('li').find('ul').html(data);
            }
        });
    });

    $('#acti-noti').closest('li').on('show.bs.dropdown', function () {
        var element = $('#acti-noti');
        element.closest('li').find('ul').html('<li style="text-align:center;padding:10px;"><img src="' + base_url + 'assets/images/loader.gif" /></li>');

        var target = element.attr('href');
        $.ajax({
            type: 'POST',
            url: target,
            data: '',
            success: function (data) {
                $.ajax({
                    type: 'POST',
                    url: target + '?seen=true',
                    data: '',
                    success: function (response) {
                        element.find('span').html('');
                    }
                });
                element.closest('li').find('ul').html(data);
            },
            error: function () {
                element.closest('li').find('ul').html(data);
            }
        });
    });

    $('.wrapper').on('click', '.toggle-comment-box', function () {
        $(this).siblings('.view-add-comment').slideToggle();
    });

    $(".post-comment").keyup(function (e) {
        adaptiveheight(this);
    });

    $(".post-comment").keypress(function (e) {
        var code = (e.which ? e.which : e.keyCode);
        if (code == 13) {
            var element = $(this);
            element.attr('readonly', 'readonly');

            element.closest('form').ajaxSubmit({
                success: function (data) {
                    if (data != '') {
                        element.closest('.media').before(data);
                        element.val('');
                    }

                    element.removeAttr('readonly');
                }
            });
        }
    });

    $(".header-top").on('click', '.noti_pop', function () {
        var target = $(this).attr('href');
        if (target == '#') {
            return false;
        }
        $.ajax({
            type: 'POST',
            url: target,
            data: '',
            success: function (data)
            {
                $('#generalModal').find('.modal-body').html(data);
                $('#generalModal').modal({
                    keyboard: false,
                });
                $('#generalModal').on('shown.bs.modal', function () {
                    $('#generalModal .modal-footer #modal-btn').hide();
                    $('#generalModal').find('.modal-body').stop().animate({scrollTop: $(".modal-body")[0].scrollHeight}, 1800);
                });

            }
        });
        return false;
    });

    // When a user is using the textarea 
    $("#form-details").bind("focus", function (e) {
        // Show the span info
        $("#form-details-info").show();
    });

    // When a user clicks the close link
    $("#form-details-info a").bind("click", function (e) {
        // Hide the info
        $("#form-details-info").hide();

        // And use this to stop a prevent a link from doing what it normally does..
        e.preventDefault();
    });

    $('.chkbox_menu label').click(function () {
        var notification = true;
        var element = $(this);
        if ($(this).children('i').hasClass('fa-check')) {
            notification = false;
        } else {
            notification = true;
        }

        $.ajax({
            type: "POST",
            url: base_url + 'profile/set_notification',
            data: "notification=" + notification,
            success: function (msg) {
                if (notification) {
                    element.children('i').removeClass('fa-remove').addClass('fa-check');
                } else {
                    element.children('i').removeClass('fa-check').addClass('fa-remove');
                }
            }
        });
    });

    $(".pop-up").on('click', function () {
        var target = $(this).attr('href');
        $.ajax({
            type: 'POST',
            url: target,
            data: '',
            success: function (data)
            {
                $('.modal-body').html(data);
                $('#myModal').modal({
                    keyboard: false,
                });
                $('#myModal').on('shown', function () {
                    $('.modal-body').stop().animate({scrollTop: $(".modal-body")[0].scrollHeight}, 1800);
                    $('#message').focus();
                });

            }
        });
        return false;
    });
    
    $(".wrapper").on('click', '.btn-pop-up', function(){
        var target = $(this).attr('href');
        $.ajax({
            type: 'POST',
            url: target,
            data: '',
            success: function(data) 
            {
                $('#generalModal').find('.modal-body').html(data);
                $('#generalModal').modal({
                    keyboard: false,
                });
                $('#generalModal').on('shown.bs.modal', function () {
                    $('#generalModal .modal-body').stop().animate({ scrollTop: $("#generalModal .modal-body")[0].scrollHeight }, 1800);
                    $('#message').focus();
                });

            }
        });
        return false;
    });
    $('body').on('click', '#modal-btn' ,function() {
        if(!$(this).parents('#generalModal').find('form').valid()) {
            return false;
        } else {
            $(this).parents('#generalModal').find('form').submit();
        }
    });    
    
    $('.follow-anchr').click(function () {
        $(this).closest('form').submit();
        return false;
    });
    $('.wrapper').on('submit', '.follow-form', function () {
        var element = $(this);
        $(this).ajaxSubmit({
            success: function (data) {
                if (data == 'following' || data == 'already') {
                    if (element.closest('li.media').length > 0) {
                        element.closest('li.media').remove();
                    } else {

                        var btn_text = element.find('.btn-text').text();
                        var new_text = btn_text.replace('Show Interest In', 'You are Interested In');
                        var name = btn_text.replace('Show Interest In ', '');

                        send_toastr_noti('success', 'Success', 'You successfully showed interest in ' + usr_name);

                        element.find('button').removeClass('btn-success').addClass('following-btn btn-info');

                        element.find('.btn-text').text(new_text);
                        element.children('input').attr('name', 'unfollow');
                        element.find('i.fa').removeClass('fa-thumbs-up').addClass('fa-heart');
                    }

                } else if (data == 'follow') {
                    var btn_text = element.find('.btn-text').text();

                    var new_text = '';
                    var usr_name = '';
                    if (btn_text.search('Cancel Interest In') > -1) {
                        new_text = btn_text.replace('Cancel Interest In', 'Show Interest In');
                        usr_name = btn_text.replace('You are Interested In ', '');
                    } else {
                        new_text = btn_text.replace('You are Interested In', 'Show Interest In');
                        usr_name = btn_text.replace('Cancel Interest In ', '');
                    }

                    send_toastr_noti('success', 'Success', 'You successfully cancelled interest in ' + usr_name);

                    element.children('button').removeClass('following-btn btn-danger btn-info').addClass('btn-success');
                    element.find('i.fa').removeClass('fa-thumbs-down fa-heart').addClass('fa-thumbs-up');
                    element.find('.btn-text').text(new_text);
                    element.children('input').attr('name', 'follow');
                    element.children('button').unbind('mouseover').unbind('mouseout');
                } else {
                    send_toastr_noti('error', 'Error', 'You can\'t show an interest in the same gender as yours');
                }
            }
        });
        return false;
    });
    $('.wrapper').on('submit', '.follow-form-refresh', function () {
        var element = $(this);
        $(this).ajaxSubmit({
            success: function (data) {
                window.location.reload();
            }
        });

        return false;
    });
    $('.wrapper').on('mouseover', '.following-btn', function () {
        var element = $(this);
        var btn_text = element.find('.btn-text').text();

        var new_text = btn_text.replace('You are Interested In', 'Cancel Interest In');
        element.addClass('btn-danger');
        element.find('.btn-text').text(new_text);
        element.find('i.fa').removeClass('fa-heart').addClass('fa-thumbs-down');
    });
    $('.wrapper').on('mouseout', '.following-btn', function () {
        var element = $(this);
        var btn_text = element.find('.btn-text').text();
        var new_text = btn_text.replace('Cancel Interest In', 'You are Interested In');
        element.removeClass('btn-danger');
        element.find('.btn-text').text(new_text);
        element.find('i.fa').removeClass('fa-thumbs-down').addClass('fa-heart');
    });

    if ($('#page_name').length > 0 && $('#page_name').val() == 'home') {
        var last_call = '';

        $(window).scroll(function () {
            if ($(window).scrollTop() + $(window).height() == $(document).height()) {
                if (!($('.posts_footer').children('h4').length > 0) && $('.post_time').last().length > 0) {
                    var last_time = $('.post_time').last().val();
                    if (last_time != last_call) {

                        $('.posts_footer #loading').show();
                        last_call = last_time;
                        $.ajax({
                            type: "get",
                            url: base_url + "members/index",
                            data: "time=" + last_time,
                            success: function (data) {
                                if (data != '') {
                                    $('.posts').append(data);
                                } else {
                                    $('.posts_footer').html('<h4><b>No More Posts</b></h4>');
                                }

                                $('.posts_footer #loading').hide();
                            }
                        });
                    }
                }
            }
        });

        if ($('.post_time').first().length > 0) {
            setInterval(function () {
                var first_time = $('.post_time').first().val();

                $('.posts_header #loading').show();
                $.ajax({
                    type: "get",
                    url: base_url + "members/recent_post",
                    data: "time=" + first_time,
                    success: function (data) {
                        if (data != '') {
                            $('.posts').prepend(data);
                        }

                        $('.posts_header #loading').hide();
                    }
                });
            }, 20000);
        }
    }

    /*new*/
    $('.delete-post').on('submit', function () {
        var element = $(this);
        $(this).ajaxSubmit({
            success: function (data) {
                if (data == 'deleted') {
                    element.parent('.post').remove();
                } else {
                    alert("Something went wrong, please try again after some time");
                }
            }
        });
        return false;
    });

    $('.post').on('mouseover', function () {
        $(this).addClass('raised');
        $(this).children('.delete-post').show();
    });

    $('.post').on('mouseout', function () {
        $(this).removeClass('raised');
        $(this).children('.delete-post').hide();
    });

    $('.upload_pro_pic').on('submit', function () {
        var element = $(this);
        element.children('img').show();
        $(this).ajaxSubmit({
            success: function (data) {
                element.children('img').hide();
                element.children('.label-success').show().delay(1500).hide(0);
                window.location.reload();
            }
        });
        return false;
    });

    $('.uploadProPicBtn').click(function (event) {
        $(this).closest('form').find('.input_pp').trigger('click');
        event.preventDefault();
    });

    $('.input_pp').change(function () {
        $(this).parent('form').submit();
    });

    $('.uploadFile').click(function (event) {
        alert(2);
        $(this).siblings('.input_picture').trigger('click');

        event.stopPropagation();
        event.preventDefault();
    });

    /* $('.image-divs').click(function (event) {
        alert(1);
        if ($(event.target).is('form')) {
            $(this).children('a').trigger('click');
            return;
        } else if ($(event.target).is('div')) {
            $(this).children('a').trigger('click');
            return;
        }
    }); */
});

function adaptiveheight(a) {
    $(a).height(0);
    var scrollval = $(a)[0].scrollHeight;
    $(a).height(scrollval);
    if (parseInt(a.style.height) > $(window).height() - 30) {
        $(document).scrollTop(parseInt(a.style.height));
    }
}

function send_toastr_noti(type, title, message) {
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "positionClass": "toast-top-right",
        "onclick": null,
        "showDuration": "1000",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    toastr[type](message, title)
}

function notification() {
    var base_url = $('#base_url').val();
    $.ajax({
        type: "get",
        url: base_url + "members/notification",
        data: "",
        dataType: "json",
        success: function (data) {
            if (data.activities > 0) {
                $('#acti-noti').find('span').text(data.activities);
            } else {
                $('#acti-noti').find('span').text('');
            }

            if (data.messages > 0) {
                $('#msg-noti').find('span').text(data.messages);
            } else {
                $('#msg-noti').find('span').text('');
            }
        }
    });
}

