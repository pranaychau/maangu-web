<div class="row">                
    <div class="col-lg-12">
        <h2 class="page-header text-center"><?php echo count($viewers); ?> people viewed your profile in the past 30 days</h2>
    </div>
    <div class="col-sm-3">
        <div class="admin-sidebar">
            <h4 class="titleHeader">Sponsored</h4>
            <div class="sidebar-content">
                <img src="<?php echo url::base(); ?>assets/images/adds/300x250.gif" class="img-responsive center-block"/>
            </div>
            <hr>
            <h4 class="titleHeader">Sponsored</h4>
            <div class="sidebar-content">
                <img src="<?php echo url::base(); ?>assets/images/adds/300x250.gif" class="img-responsive center-block"/>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="boxContent-inner">
            <?php
            $i = 1;
            foreach ($viewers as $viewer) {
                if ($i != 1) {
                    ?>
                    <hr/>
                <?php } ?>
                <div class="row"> 
                    <div class="col-md-3 col-sm-4 col-xs-3">
                        <div class="list-view-img">
                            <a href="<?php echo url::base() . $viewer->viewer->user->username; ?>" class="noMargin">
                                <?php if ($viewer->viewer->photo->profile_pic_s) { ?>
                                    <img src="<?php echo url::base() . 'upload/' . $viewer->viewer->photo->profile_pic_s; ?>">
                                <?php } else { ?>
                                   <div class="sm" id="inset">
                                        <h1 class="margin-top-0px">
                                            <?php
                                            $parts = explode(" ", $viewer->viewer->get_name());
                                            echo substr($parts[0], 0, 1);
                                            echo substr($parts[1], 0, 1);
                                            ?>
                                        </h1>
                                    </div>
                                <?php } ?>
                            </a>
                        </div>
                    </div>
                   <div class="col-md-5 col-sm-4 col-xs-9">
                        <h4 class="text-info">
                            <?php
                            $age = date_diff(DateTime::createFromFormat('Y-m-d', $viewer->viewer->birthday), date_create('now'))->y;
                            ?>
                            <i class="fa fa-check-circle text-success"></i> 
							<?php
                                    $parts = explode(" ", $viewer->viewer->get_name());
									   
                                    echo substr($parts[0], 0);
                                    echo "\n";									
                                  	echo substr($parts[1], 0);							                                   
							?>
                            (<?php echo $age; ?>)
                        </h4>
                        <?php

                        

                        $display_details = array();
						
                        if (!empty($viewer->viewer->marital_status)) {
                            $detail = Kohana::$config->load('profile')->get('marital_status');
                            $display_details[] = $detail[$viewer->viewer->marital_status];
                        }

                         $display_details[] = $viewer->viewer->location;
                        
                        foreach (Kohana::$config->load('profile')->get('religion') as $key => $value) {
                            if ($viewer->viewer->religion == $key) {
                                $rel = $value;
                            }
                        }
                        
                        
                        foreach (Kohana::$config->load('profile')->get('caste') as $key => $value) {
                            if ($viewer->viewer->caste == $key) {
                                $caste = $value;
                            }
                        }
                        
                        $display_details[] = $rel .', ' .$caste;
                        

                        $Address2 = urlencode($viewer->viewer->location);
                        $request_url2 = "http://maps.googleapis.com/maps/api/geocode/xml?address=" . $Address2 . "&sensor=true";
                        $xml2 = simplexml_load_file($request_url2) or die("url not loading");
                       
                        ?>
                        <p class="text-info">
                        <?php echo implode('<br /> ', $display_details); ?>
                        </p>
                    </div>
                    <!--                    <div class="col-md-4 col-sm-4 col-xs-12">
                                            <div class="top30"></div>
    <?php echo View::factory('templates/commons/interest_button', array('member' => $viewer->viewer)); ?>
                                        </div>-->
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="top30"></div>						
                        <form class="mb-19px follow-form"  action="<?php echo url::base() . "members/follow" ?>" method="post">
                            <input type="hidden" class="f-f_id" name="follow" value="<?php echo $viewer->viewer->user->member->id; ?>"/>
                            <button type="submit" class=" btn btn-labeled btn-success marginVertical">
                                <span class="btn-label"><i class="fa fa-thumbs-up"></i> </span>Show Interest
								
                            </button>
                        </form>
                    </div>
                </div><!-- /.row -->
<?php } ?>
        </div>
    </div>                
    <div class="col-sm-3">
        <div class="admin-sidebar">
            <h4 class="titleHeader">Sponsored</h4>
            <div class="sidebar-content">
                <img src="<?php echo url::base(); ?>assets/images/adds/300x250.gif" class="img-responsive center-block"/>
            </div>
            <hr>
            <h4 class="titleHeader">Sponsored</h4>
            <div class="sidebar-content">
                <img src="<?php echo url::base(); ?>assets/images/adds/300x250.gif" class="img-responsive center-block"/>
            </div>
            <hr/>
            <div class="sidebar-content">
<?php echo View::factory('sidebars/links'); ?>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'address': '<?php echo $member->location; ?>'}, function (results, status) {
            var latitude1 = results[0].geometry.location.lat();
            var longitude1 = results[0].geometry.location.lng();
            $('#us2-lat').val(latitude1);
            $('#us2-lon').val(longitude1);
        });

    });

    var latitude2 = document.getElementById("us2-lat").value;
    var longitude2 = document.getElementById("us2-lon").value;
    $('#us2').locationpicker({
        location: {latitude: latitude2, longitude: longitude2},
        radius: 300
    });

</script>