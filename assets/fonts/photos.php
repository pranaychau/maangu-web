<script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

 <script type='text/javascript' src="//netdna.bootstrapcdn.com/bootstrap/3.0.0-rc1/js/bootstrap.min.js"></script>
<!--      <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0-rc1/css/bootstrap.min.css" rel="stylesheet">-->
<script type="text/javascript">
    $(document).ready(function () {
        /*
         *  Simple image gallery. Uses default settings
         */
        $('.image-divs').click(function (event)
        {
            $(this).siblings('a').trigger('click');
        });
    
        $('.thumbnailsds').click(function()
        {
       $('.modal-body').empty();
  	//var title = $(this).parent('a').attr("title");
        var title1="Image Galary"
  	$('.modal-title').html(title1);
        
  	$($(this).parents('div').html()).appendTo('.modal-body');
  	$('#myModal').modal({show:true});



    });
    });
</script>






<?php $current_member = Auth::instance()->get_user()->member;?>
<link rel="stylesheet" type="text/css" href="<?php echo url::base(); ?>assets/plugins/colorbox/colorbox.css" />
<div class="borderBox top20">
    <h3 class="noMargin">Photos</h3>
    <hr>
     
    <div class="row marginTop">
        <div class="col-lg-12">

            <div class="row">
                <div class='list-group gallery paddingTop'>

                    <div class='col-sm-4 col-xs-6'>
                        <div class="thumbnail img-div">
                            <?php if ($member->photo->picture1) 
                                  { ?>
                                         <div class="image-divs" >
                                        <a title="<?php echo url::base() . "upload/original/" . $member->photo->picture1;?>"  class="lod">
                                            <img src="<?php echo url::base() . "upload/original/" . $member->photo->picture1;?>" class="thumbnailsds img-responsive" style="height:100%">
                                        </a>
                                   
                                         </div >
                            <?php } else
                                     { ?>
                                        <div class="image-divs image-default" > </div>
                                     <?php } ?>
                               
                        
                            <div class=" text-center">
                                <?php if ($current_member->id === $member->id) { ?>
                                    <form class="upload_extra_pic dis-in-block" enctype="multipart/form-data" action="<?php echo url::base() . "profile/add_pic" ?>" method="POST">
                                        <input class="input_picture" name="picture" type="file"/>
                                        <input type="hidden" name="name" value="1" />

                                        <button class="btn btn-primary uploadPicBtn btn-xs" type="submit">
                                            <i class="fa fa-upload"></i> Upload
                                        </button>

                                    </form>
                                <?php } ?>

                                <?php if ($member->photo->picture1 && (($current_member->id == $member->id) || (Auth::instance()->logged_in('admin'))))
                                    { ?>
                                    <form class="delete_pic dis-in-block" action="<?php echo url::base() . "profile/delete_pic" ?>" method="POST">
                                        <input type="hidden" name="name" value="1" />
                                        <input type="hidden" name="user_image_id" value="<?php echo $member->id; ?>" />
                                        <button class="btn btn-danger deletePic btn-xs" type="submit">
                                            <i class="fa fa-trash"></i> Delete
                                        </button>
                                    </form>
                                <?php } ?>
                            </div>

                                <?php if (isset($member->photo->picture1) && !empty($member->photo->picture1))
                                      { ?>
                                          <a class="member_img"  href="<?php echo url::base() . "upload/original/" . $member->photo->picture1; ?>"></a>
                                    <?php } ?>
                        </div>

                        <img class="image-loader" src="<?php echo url::base() . "assets/images/loader.gif"; ?>" style="display:none;">
                    </div> <!-- col-4 / end -->
                    
                    
                    <div class='col-sm-4 col-xs-6'>
                        <div class="thumbnail img-div">
                            <?php if ($member->photo->picture2) 
                                  { ?>
                            <div class="image-divs" >
                                  <a title="<?php echo url::base() . "upload/original/" . $member->photo->picture2;?>"  class="lod">
                                      <img src="<?php echo url::base() . "upload/original/" . $member->photo->picture2;?>" class="thumbnailsds img-responsive" style="height:70%">
                                  </a>
                                   
                            </div >
                            <?php } else
                                { ?>
                                <div class="image-divs image-default"> </div>
                            <?php } ?>
                              
                        
                            <div class=" text-center">
                                <?php if ($current_member->id === $member->id) { ?>
                                    <form class="upload_extra_pic dis-in-block" enctype="multipart/form-data" action="<?php echo url::base() . "profile/add_pic" ?>" method="POST">
                                        <input class="input_picture" name="picture" type="file"/>
                                        <input type="hidden" name="name" value="2" />

                                        <button class="btn btn-primary uploadPicBtn btn-xs" type="submit">
                                            <i class="fa fa-upload"></i> Upload
                                        </button>

                                    </form>
                                <?php } ?>

                                <?php if ($member->photo->picture2 && (($current_member->id == $member->id) || (Auth::instance()->logged_in('admin'))))
                                    { ?>
                                    <form class="delete_pic dis-in-block" action="<?php echo url::base() . "profile/delete_pic" ?>" method="POST">
                                        <input type="hidden" name="name" value="2" />
                                        <input type="hidden" name="user_image_id" value="<?php echo $member->id; ?>" />
                                        <button class="btn btn-danger deletePic btn-xs" type="submit">
                                            <i class="fa fa-trash"></i> Delete
                                        </button>
                                    </form>
                                <?php } ?>
                            </div>

                                <?php if (isset($member->photo->picture2) && !empty($member->photo->picture2))
                                      { ?>
                                          <a class="member_img"  href="<?php echo url::base() . "upload/original/" . $member->photo->picture2; ?>"></a>
                                    <?php } ?>
                       

                        <img class="image-loader" src="<?php echo url::base() . "assets/images/loader.gif"; ?>" style="display:none;">
                        </div>
                        </div> <!-- col-4 / end -->
                    
                    
                    <div class='col-sm-4 col-xs-6'>
                        <div class="thumbnail img-div">
                            <?php if ($member->photo->picture3) 
                                  { ?>
                            <div class="image-divs" >
                                  <a title="<?php echo url::base() . "upload/original/" . $member->photo->picture3;?>"  class="lod">
                                      <img src="<?php echo url::base() . "upload/original/" . $member->photo->picture3;?>" class="thumbnailsds img-responsive" style="height:70%">
                                  </a>
                                   
                            </div >
                            <?php } else
                                { ?>
                                <div class="image-divs image-default"> </div>
                            <?php } ?>
                                
                        
                            <div class=" text-center">
                                <?php if ($current_member->id === $member->id) { ?>
                                    <form class="upload_extra_pic dis-in-block" enctype="multipart/form-data" action="<?php echo url::base() . "profile/add_pic" ?>" method="POST">
                                        <input class="input_picture" name="picture" type="file"/>
                                        <input type="hidden" name="name" value="3" />

                                        <button class="btn btn-primary uploadPicBtn btn-xs" type="submit">
                                            <i class="fa fa-upload"></i> Upload
                                        </button>

                                    </form>
                                <?php } ?>

                                <?php if ($member->photo->picture3 && (($current_member->id == $member->id) || (Auth::instance()->logged_in('admin'))))
                                    { ?>
                                    <form class="delete_pic dis-in-block" action="<?php echo url::base() . "profile/delete_pic" ?>" method="POST">
                                        <input type="hidden" name="name" value="3" />
                                        <input type="hidden" name="user_image_id" value="<?php echo $member->id; ?>" />
                                        <button class="btn btn-danger deletePic btn-xs" type="submit">
                                            <i class="fa fa-trash"></i> Delete
                                        </button>
                                    </form>
                                <?php } ?>
                            </div>

                                <?php if (isset($member->photo->picture1) && !empty($member->photo->picture1))
                                      { ?>
                                          <a class="member_img"  href="<?php echo url::base() . "upload/original/" . $member->photo->picture3; ?>"></a>
                                    <?php } ?>
                        </div>

                        <img class="image-loader" src="<?php echo url::base() . "assets/images/loader.gif"; ?>" style="display:none;">
                    </div> <!-- col-4 / end -->
                   
                   
                </div> <!-- list-group / end -->
            </div> <!-- row / end -->
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo url::base();?>assets/plugins/colorbox/jquery.colorbox-min.js"></script>


<div id="myModal" class="modal fade" tabindex="-1" role="dialog" >
  <div class="modal-dialog">
  <div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">×</button>
		<h3 class="modal-title">Heading</h3>
	</div>
	<div class="modal-body">
		
	</div>
	<div class="modal-footer">
		<button class="btn btn-default" data-dismiss="modal">Close</button>
	</div>
   </div>
  </div>
</div>
<style>
    
   
.thumbnail {margin-bottom:6px;}
.modal-body{height: 400px}
</style>